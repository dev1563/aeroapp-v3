import {Linking, PermissionsAndroid, Platform} from 'react-native';

import messaging from '@react-native-firebase/messaging';
import notifee, {EventType} from '@notifee/react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';

import {TTemasNotificaciones} from '../../types/TTemasNotificaciones';

let setNotificationsCant: any;

const NotificationsData: any = {
  unseen: 0,
  notifications: {},
  orden: [],
};

const configSetNotificationsCant = (setNotificationsCantRecibed: any) => {
  setNotificationsCant = setNotificationsCantRecibed;
};

const inicioServicioNotificaciones = async () => {
 // const authStatus = await messaging().requestPermission();

  

  // Obtiene y el token actual de notificaciones
  return await getTokenNotificaciones();
};

const suscribirAtema = async (tema: TTemasNotificaciones) => {
  try {
    await messaging().subscribeToTopic(tema);
  } catch (e) {}
};

const eliminarDetema = async (tema: TTemasNotificaciones) => {
  try {
    await messaging().unsubscribeFromTopic(tema);
  } catch (e) {}
};

// Se usa cuando se cierra sesión por parte del usuario
const generarNuevoToken = async () => {
  try {
    await messaging().deleteToken();
  } catch (e) {}
  await limpiarNotificacionesHistorial();
  return await getTokenNotificaciones(); // Genera el nuevo token
};

const getTokenNotificaciones = async () => {
  let tokenActual;
  try {
    tokenActual = await messaging().getToken();
  } catch (e) {
    return '';
  }

  let tokenNotificaciones =
    (await AsyncStorage.getItem('tokenNotificaciones')) || '';

  if (tokenActual !== tokenNotificaciones) {
    // Existe un cambio en el token de notificaciones
    // Si esta logueado el usuario como usuario se debe actualizar
    await AsyncStorage.setItem('tokenNotificaciones', tokenActual);
    suscribirAtema('all');
    getTokenNotificaciones();
  } else {
    return tokenActual;
  }
};

const marcarNotiCompleta = async (id_notificacion: string) => {
  let dataActualNoti = await getListNotifications();

  if (Object.keys(dataActualNoti.notifications).includes(id_notificacion)) {
    dataActualNoti.notifications[id_notificacion].leido = true;
    dataActualNoti.unseen = dataActualNoti.unseen - 1;

    await AsyncStorage.setItem(
      'dataNotificaciones',
      JSON.stringify(dataActualNoti),
    );

    updateCantNotifications();

    return;
  } else {
    setTimeout(() => {
      marcarNotiCompleta(id_notificacion);
    }, 1160);
  }
};

const aperturaNotificacion = async (notificacion: any) => {
  // Se realizo la apertura de la notificación, segun el payload hace el registro y o redirección
  // Detectar si tiene una url
  console.log(notificacion.data)
  // La marca como completa
  marcarNotiCompleta(notificacion.data.id_notification);

  if (notificacion.data.url && notificacion.data.url !== '') {
    Linking.openURL(notificacion.data.url);
  }
};

const foregroundNotifications = async () => {
  const unsubscribe = messaging().onMessage(async remoteMessage => {
    const guradarNoti = await guardarHistorialNotificaciones(remoteMessage);

    const channelId = await notifee.createChannel({
      id: 'default',
      name: 'Default',
    });
    
    // Muestra la notificación así la app este abierta
    await notifee.displayNotification({
      title: remoteMessage.notification?.title,
      body: remoteMessage.notification?.body,
      android: {
        channelId,
        //smallIcon: 'ic_notification',
        color: '#e7541c',
      },
      data: remoteMessage.data,
    });
  });

  // Detecat si es abirta una notificacion generada localmente
  notifee.onForegroundEvent(({type, detail}) => {
    switch (type) {
      case EventType.DISMISSED:
        break;
      case EventType.PRESS:
        let notificationFormation = {
          notification: detail.notification,
          data: detail.notification?.data,
        };
        aperturaNotificacion(notificationFormation);
        break;
    }
  });

  return unsubscribe;
};

const notificacionAbierta = async () => {
  // Hizo que la aplicación se abriera desde el background
  messaging().onNotificationOpenedApp(remoteMessage => {
    aperturaNotificacion(remoteMessage);
  });

  messaging()
    .getInitialNotification()
    .then(remoteMessage => {
      if (remoteMessage) {
        aperturaNotificacion(remoteMessage);
      }
    });
};

const getListNotifications = async () => {
  const dataNotificaciones =
    (await AsyncStorage.getItem('dataNotificaciones')) || '';

  return dataNotificaciones === ''
    ? NotificationsData
    : JSON.parse(dataNotificaciones);
};

const guardarHistorialNotificaciones = async (notificacion: any) => {
  // Guarda la notificacion y la guarda como no leida y en el ultimo espacio de lectura
  let nuevaDataNotificacion = await getListNotifications();
  nuevaDataNotificacion.notifications[notificacion.data.id_notification] = {};
  nuevaDataNotificacion.notifications[notificacion.data.id_notification].fecha =
    new Date().getTime();
  nuevaDataNotificacion.notifications[notificacion.data.id_notification].leido =
    false;
  nuevaDataNotificacion.notifications[
    notificacion.data.id_notification
  ].notification = notificacion;
  nuevaDataNotificacion.unseen = nuevaDataNotificacion.unseen + 1;
  nuevaDataNotificacion.orden.push(notificacion.data.id_notification);

  // Guarda el registro
  await AsyncStorage.setItem(
    'dataNotificaciones',
    JSON.stringify(nuevaDataNotificacion),
  );

  updateCantNotifications();

  return true;
};

const limpiarNotificacionesHistorial = async () => {
  await AsyncStorage.setItem(
    'dataNotificaciones',
    JSON.stringify(NotificationsData),
  );
  updateCantNotifications();
};

const updateCantNotifications = async () => {
  const dataNotifications = await getListNotifications();

  if (setNotificationsCant) {
    setNotificationsCant(dataNotifications.unseen);
  }
};

const marcarTododLeido = async () => {
  let nuevaDataNotificacion = await getListNotifications();
  nuevaDataNotificacion.unseen = 0;
  for (
    let i = 0;
    i < Object.keys(nuevaDataNotificacion.notifications).length;
    i++
  ) {
    nuevaDataNotificacion.notifications[
      Object.keys(nuevaDataNotificacion.notifications)[i]
    ].leido = true;
  }

  // Guarda el registro
  await AsyncStorage.setItem(
    'dataNotificaciones',
    JSON.stringify(nuevaDataNotificacion),
  );

  updateCantNotifications();
};

export {
  inicioServicioNotificaciones,
  suscribirAtema,
  eliminarDetema,
  generarNuevoToken,
  getTokenNotificaciones,
  foregroundNotifications,
  notificacionAbierta,
  marcarNotiCompleta,
  getListNotifications,
  guardarHistorialNotificaciones,
  configSetNotificationsCant,
  updateCantNotifications,
  marcarTododLeido,
  limpiarNotificacionesHistorial,
  aperturaNotificacion,
};
