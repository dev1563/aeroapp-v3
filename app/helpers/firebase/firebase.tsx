import remoteConfig from '@react-native-firebase/remote-config';
import AsyncStorage from '@react-native-async-storage/async-storage';

async function fetchRemoteConfig() {
    try {
        await remoteConfig().activate();
        await remoteConfig().fetch(24 * 60 * 60 * 1000); // Cachear durante 24 horas
        const parameters = remoteConfig().getAll();
        await AsyncStorage.clear();

        await Promise.all(
            Object.entries(parameters).map(async ($) => {
                const [key, entry] = $;
                await AsyncStorage.setItem(key, entry.asString());
            })
        );

        return { isFetched: true, error: null };
    } catch (error) {
        return { isFetched: false, error };
    }
}

async function getRemoteConfig<T>(key: string) {
    try {
        const data = await AsyncStorage.getItem(key);
        if (data) {
            return data;
        } else {
            throw new Error(`Remote Config value for key ${key} not found`);
        }
    } catch (errors) {
        // Si no encuentra el valor, intenta obtenerlo nuevamente llamando a fetchRemoteConfig
        const { isFetched, error } = await fetchRemoteConfig();

        if (isFetched) {
            const newData = await AsyncStorage.getItem(key);
            if (newData) {
                return newData;
            } else {
                throw new Error(`Remote Config value for key ${key} still not found`);
            }
        } else {
            throw errors;
        }
    }
}

export { fetchRemoteConfig, getRemoteConfig };
