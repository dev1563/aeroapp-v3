import { useEffect, useState } from 'react';
import {
  Pressable,
  StyleSheet,
  Text,
  Platform,
  Dimensions,
  View,
} from 'react-native';


import Animated, {
  useAnimatedStyle,
  useSharedValue,
  withTiming,
} from 'react-native-reanimated';

import { theme } from '../../theme/theme';
import { getRemoteConfig } from '../../helpers/firebase/firebase';

export function Boton(props: any) {
  // El estilo del boton es cambiante
  const [pressedBtn, setPressedBtn] = useState(false);
  const [estadocCarga, setEstadocarga] = useState(false);
  const [opacityTxtBtn, setopacityTxtBtn] = useState(1);
  const windowWidth = Dimensions.get('window').width;
  const anchoBoton = useSharedValue(windowWidth);

  const [primaryColor, setPrimaryColor] = useState(theme().root.bb_white);
  const [fontColorPrimary, setfontColorPrimary] = useState(theme().root.bb_white);
  getRemoteConfig('primaryColor')
    .then((value: any) => {
      setPrimaryColor(value)
    })
    .catch((error) => {
      console.log(error);
    });
  getRemoteConfig('fontColorPrimary')
    .then((value: any) => {
      setfontColorPrimary(value)
    })
    .catch((error) => {
      console.log(error);
    });

  useEffect(() => {
    setEstadocarga(props.cargando || false);

    anchoBoton.value = withTiming(props.cargando ? 116 : windowWidth, {
      duration: 216,
    });

    if (props.cargando === false) {
      setTimeout(() => {
        setopacityTxtBtn(1);
      }, 170);
    } else {
      setopacityTxtBtn(0);
    }

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [props.cargando]);

  const animatedStyleBtnAncho = useAnimatedStyle(() => {
    return {
      width: anchoBoton.value,
    };
  });

  const handlePressIn = () => {
    setPressedBtn(true);
  };

  const handlePressOut = () => {
    setPressedBtn(false);
  };

  const style = StyleSheet.create({
    contenedorBoton: {
      width: windowWidth,
      maxWidth: '100%',
    },
    pressable: {
      height: props.height || 46,
      backgroundColor: props.disable
        ? theme().root.bb_white
        : estadocCarga
          ? fontColorPrimary
          : pressedBtn
            ? primaryColor
            : primaryColor,
      opacity: estadocCarga ? 0.6 : 1,
      borderRadius: 50,
      alignItems: 'center',
      justifyContent: 'center',
      borderWidth: props.disable ? 1 : 0,
    },
    shadowBtn: {
      shadowColor: '#6a7784',
      shadowOffset: {
        width: 0,
        height: 1,
      },
      shadowOpacity: 0.4,
      shadowRadius: 8,
      elevation: 2,
    },
    textoBoton: {
      fontFamily: theme().root.bb_font_regular,
      color: props.disable ? theme().root.bb_gray : fontColorPrimary,
      fontSize: props.fontSize || 18,
      opacity: opacityTxtBtn,
    },
    spinner: {
      marginTop: Platform.OS === 'ios' ? -10 : 0,
    },
  });

  return (
    <View style={{ width: '100%', alignItems: 'center' }}>
      <Animated.View style={[style.contenedorBoton, animatedStyleBtnAncho]}>
        <Pressable
          {...props}
          onPressIn={handlePressIn}
          onPressOut={handlePressOut}
          disabled={estadocCarga || props.disable}
          style={[style.pressable, pressedBtn && style.shadowBtn]}>
          {estadocCarga ? (
           <Text/>
          ) : (
            <Text style={style.textoBoton}>{props.nombreBtn || ''}</Text>
          )}
        </Pressable>
      </Animated.View>
    </View>
  );
}

export default Boton;
