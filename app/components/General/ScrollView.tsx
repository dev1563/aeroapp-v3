import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';

export function ScrollView(props: any) {
  return (
    <KeyboardAwareScrollView
      {...props}
      scrollEventThrottle={16}
      showsVerticalScrollIndicator={props.showsVerticalScrollIndicator || false}
      keyboardShouldPersistTaps="handled"
      enableOnAndroid={true}
      extraScrollHeight={70}>
      {props.children}
    </KeyboardAwareScrollView>
  );
}

export default ScrollView;
