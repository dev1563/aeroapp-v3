import React, { useState } from 'react';
import { View, Text, Image, Pressable, StyleSheet, Linking, Alert } from 'react-native';
import { theme } from '../../theme/theme';
import { useSafeAreaInsets } from 'react-native-safe-area-context';
import { getRemoteConfig } from '../../helpers/firebase/firebase';
import InAppBrowser from 'react-native-inappbrowser-reborn';
import { useTranslation } from 'react-i18next';

const DynamicCategory = ({ categoryData, fontPrimary }: any) => {
    const { top } = useSafeAreaInsets();
    const [fontColorPrimary, setfontColorPrimary] = useState(theme().root.bb_white);
    

    getRemoteConfig('fontColorSecundary')
        .then((value: any) => {
            setfontColorPrimary(value)
        })
        .catch((error) => {
            console.log(error);
        });

    async function sleep(timeout: number) {
        return new Promise(resolve => setTimeout(resolve, timeout))
    }
    const openUrl = async (url: any) => {
        try {
            if (await InAppBrowser.isAvailable()) {
                const result = await InAppBrowser.open(url, {
                    // iOS Properties
                    dismissButtonStyle: 'cancel',
                    preferredBarTintColor: fontColorPrimary,
                    preferredControlTintColor: 'white',
                    readerMode: false,
                    animated: true,
                    modalPresentationStyle: 'fullScreen',
                    modalTransitionStyle: 'coverVertical',
                    modalEnabled: true,
                    enableBarCollapsing: false,
                    // Android Properties
                    showTitle: true,
                    toolbarColor: fontColorPrimary,
                    secondaryToolbarColor: 'black',
                    navigationBarColor: 'black',
                    navigationBarDividerColor: 'white',
                    enableUrlBarHiding: true,
                    enableDefaultShare: false,
                    forceCloseOnRedirection: false,

                    animations: {
                        startEnter: 'slide_in_right',
                        startExit: 'slide_out_left',
                        endEnter: 'slide_in_left',
                        endExit: 'slide_out_right'
                    },
                    headers: {
                        'my-custom-header': 'my custom header value'
                    }
                })
                await sleep(800);
            }
            else Linking.openURL(url)
        } catch (error: any) {
            Alert.alert(error.message)
        }
    };

    const Homestyles = StyleSheet.create({
        contenedorHeader: {
            zIndex: 1,
        },
        contenedorBoxi: {
            zIndex: 1,
            paddingHorizontal: 16,
            marginTop: top + 140,
            position: 'absolute',
            width: '100%',
        },
        scrollContenedor: {
            marginTop: top + 120,
            paddingHorizontal: 16,
            paddingVertical: 20,
        },
        contenedorCategories: {
            flex: 1,
            width: '100%',
        },
        titleContenedorCategories: {
            display: 'flex',
            alignContent: 'flex-start',
            marginBottom: 10,
        },
        titleCategories: {
            width: '100%',
            fontFamily: theme().root.bb_font_bold,
            color: theme().root.bb_purple,
            fontSize: 18,
            marginRight: 220,
        },
        contenedorCardCategories: {
            flex: 1,
            flexDirection: 'row',
            justifyContent: 'space-between',
            marginBottom: 20,
            width: '100%'
        },
        spanCategories: {
            width: 60,
            padding: 14,
            lineHeight: 60,
            height: 60,
            backgroundColor: fontColorPrimary,
            borderRadius: 6,
            display: 'flex',
        },
        iconCategories: {
            width: 35,
            height: 35,
            tintColor: theme().root.bb_white,
        },
        subtitleCategories: {
            position: 'absolute',
            width: 90,
            color: theme().root.bb_dark,
            fontSize: 11,
            fontFamily: theme().root.bb_font_regular,
            //padding: 5,
            marginTop: 70,
            paddingHorizontal: 10
        },
        cardHome: {
            backgroundColor: theme().root.bb_white,
            padding: 15,
            width: 170,
            height: 170,
            borderRadius: 10,
            shadowColor: '#000000',
            shadowOffset: {
                width: 0,
                height: 0,
            },
            shadowOpacity: 0.3,
            shadowRadius: 5,
            elevation: 5,
            marginHorizontal: 5,
        },
        imgVideoHome: {
            width: 140,
            height: 100,
            marginBottom: 5
        },
        cardHome2: {
            backgroundColor: theme().root.bb_white,
            padding: 15,
            width: 100,
            height: 170,
            borderRadius: 10,
            shadowColor: '#000000',
            shadowOffset: {
                width: 0,
                height: 0,
            },
            shadowOpacity: 0.3,
            shadowRadius: 5,
            elevation: 5,
            marginHorizontal: 10
        },
        imgVideoHome2: {
            width: 70,
            height: 100,
            marginRight: 10,
            marginBottom: -10,
            marginTop: -10,
        },
        btnlogin: {
            marginTop: 7,
            width: '100%',
        },
        contenedorCampo: {
            width: '95%',
            height: 58,
            backgroundColor: theme().root.bb_white,
            borderRadius: 50,
            justifyContent: 'center',
            marginBottom: 16,
            shadowColor: '#000000',
            shadowOffset: {
                width: 0,
                height: 0,
            },
            shadowOpacity: 0.3,
            shadowRadius: 5,
            elevation: 5,
            marginLeft: 10,
        },
        roundImagen: {
            width: 42,
            height: 42,
            backgroundColor: theme().root.bb_purple,
            borderRadius: 50,
            position: 'absolute',
            marginLeft: 10,
            right: 10,
            justifyContent: 'center',
            alignItems: 'center',
        },
        imageRound: {
            width: 26,
            height: 26,
            tintColor: theme().root.bb_light
        },
        titulocampo: {
            position: 'absolute',
            marginLeft: 32,
            height: 26,
            fontSize: 15,
            color: theme().root.bb_dark,
            fontFamily: theme().root.bb_font_regular,
        },
        titulocampoFocus: {
            position: 'absolute',
            marginLeft: 32,
            height: 43,
            fontSize: 10,
            color: theme().root.bb_dark,
            fontFamily: theme().root.bb_font_regular,
        },
        label: {
            marginTop: 25,
            color: 'white',
            fontWeight: 'bold'
        },
        campoTexto: {
            zIndex: 1,
            paddingLeft: 32,
            paddingRight: 16,
            //paddingTop: Platform.OS === 'ios' ? 18 : 29,
            marginTop: 18,
            color: theme().root.bb_dark,
            fontFamily: theme().root.bb_font_regular,
            fontSize: 18,
            width: '100%',
            height: 58,
        },
        contenedorCampoHeader: {
            marginTop: 60,
            marginLeft: 30,
        },
        roundImagenHeader: {
            width: 50,
            height: 50,
            backgroundColor: theme().root.bb_white,
            borderRadius: 50,
            position: 'absolute',
            marginLeft: 10,
            justifyContent: 'center',
            alignItems: 'center',
        },
        imageRoundHeader: {
            width: 40,
            height: 40,
            borderRadius: 50,
        },
        titulocampoHeader: {
            position: 'absolute',
            marginLeft: 70,
            height: 26,
            fontSize: 18,
            color: theme().root.bb_white,
            fontFamily: theme().root.bb_font_bold,
        },
        titulocampoFocusHeader: {
            position: 'absolute',
            marginLeft: 70,
            height: 26,
            marginTop: 20,
            fontSize: 16,
            color: theme().root.bb_white,
            fontFamily: theme().root.bb_font_regular,
        },
    });

    if (categoryData != undefined) {
        const { t } = useTranslation();
        return (
            <View>
                <View style={Homestyles.titleContenedorCategories}>
                    <Text style={Homestyles.titleCategories}>{t(categoryData.name)}</Text>
                </View>
                <View style={Homestyles.contenedorCardCategories}>
                    {categoryData.items.map((item: any, index: number) => (
                        <Pressable
                            key={index}
                            style={Homestyles.spanCategories}
                            onPress={() => openUrl(item.route)}
                        >
                            <Image source={item.icon} style={Homestyles.iconCategories} resizeMode="contain" />
                            <Text style={[Homestyles.subtitleCategories, { fontFamily: fontPrimary }]}>
                                {t(item.subtitleKey)}
                            </Text>
                        </Pressable>
                    ))}
                </View>
            </View>
        );
    }
};

export default DynamicCategory;
