import React, { useState } from 'react'
import { View, Text, Image, Pressable, Linking, Alert } from 'react-native';
import InAppBrowser from 'react-native-inappbrowser-reborn';
import { useTranslation } from 'react-i18next';
import { Homestyles } from '../../theme/HomeTheme';
import { theme } from '../../theme/theme';

const Offers = ({ offers }: any) => {
    const [fontColorPrimary, setfontColorPrimary] = useState(theme().root.bb_white);
    async function sleep(timeout: number) {
        return new Promise(resolve => setTimeout(resolve, timeout))
    }
    const openUrl = async (url: any) => {
        try {
            if (await InAppBrowser.isAvailable()) {
                const result = await InAppBrowser.open(url, {
                    // iOS Properties
                    dismissButtonStyle: 'cancel',
                    preferredBarTintColor: fontColorPrimary,
                    preferredControlTintColor: 'white',
                    readerMode: false,
                    animated: true,
                    modalPresentationStyle: 'fullScreen',
                    modalTransitionStyle: 'coverVertical',
                    modalEnabled: true,
                    enableBarCollapsing: false,
                    // Android Properties
                    showTitle: true,
                    toolbarColor: fontColorPrimary,
                    secondaryToolbarColor: 'black',
                    navigationBarColor: 'black',
                    navigationBarDividerColor: 'white',
                    enableUrlBarHiding: true,
                    enableDefaultShare: false,
                    forceCloseOnRedirection: false,
    
                    animations: {
                        startEnter: 'slide_in_right',
                        startExit: 'slide_out_left',
                        endEnter: 'slide_in_left',
                        endExit: 'slide_out_right'
                    },
                    headers: {
                        'my-custom-header': 'my custom header value'
                    }
                })
                await sleep(800);
            }
            else Linking.openURL(url)
        } catch (error: any) {
            Alert.alert(error.message)
        }
    };

    if (offers != undefined) {
    const { t } = useTranslation();
    return (
        <View style={Homestyles.contenedorCardCategories}>
            <Pressable onPress={() => openUrl(offers.route)}>
                <View style={Homestyles.cardHome}>
                    <Image source={{uri:offers.img}} style={Homestyles.imgVideoHome} resizeMode="cover" />
                    <Text>{t(offers.titleKey)}</Text>
                    <Text>{t(offers.paragraphKey)}</Text>
                </View>
            </Pressable>
        </View>   
    )
    }
}

export default Offers;