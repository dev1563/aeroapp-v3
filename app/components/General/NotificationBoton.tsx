import {useState, useEffect} from 'react';
import {View, Image, Text, Pressable} from 'react-native';

import {useSafeAreaInsets} from 'react-native-safe-area-context';

import {
  configSetNotificationsCant,
  updateCantNotifications,
} from '../../helpers/notifications/notificationsHelper';

import {theme} from '../../theme/theme';
//import Icon_Notifications from '../../assets/icons/icon-notification.png';
import { getRemoteConfig } from '../../helpers/firebase/firebase';

export function NotificationBoton({navigation}: any): JSX.Element {
  const {top} = useSafeAreaInsets();

  const [cantNotificaciones, setCantNotificaciones] = useState(0);
  const [fontColorPrimary, setfontColorPrimary] = useState(theme().root.bb_white);

  getRemoteConfig('fontColorPrimary')
  .then((value: any) => {
    setfontColorPrimary(value)
  })
  .catch((error) => {
    console.log(error);
  });

  useEffect(() => {
    configSetNotificationsCant(setCantNotificaciones);
    setCantNotificaciones(0);
    updateCantNotifications();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  // Componente de vista
  return (
    <Pressable
      style={{
        zIndex: 1,
        flex: 1,
        position: 'relative',
        alignItems: 'flex-end',
        marginTop: 9 + top,
        marginRight: 10,
        marginLeft: 350,
        marginBottom: 120
      }}
      onPress={() => {
        navigation.navigate('Notifications');
      }}>
      <View
        style={[
          {
            flexDirection: 'row',
            alignItems: 'center',
            borderRadius: 100,
            paddingVertical: 2,
            borderColor:
              cantNotificaciones <= 0 ? fontColorPrimary : 'transparent',
            backgroundColor:
              cantNotificaciones <= 0 ? 'transparent' : fontColorPrimary,
          },
          cantNotificaciones <= 0
            ? {}
            : {
                shadowColor: '#000000',
                shadowOffset: {
                  width: 0,
                  height: 0,
                },
                shadowOpacity: 0.2,
                shadowRadius: 4,
                elevation: 2,
              },
        ]}>
        <Image
          //source={Icon_Notifications}
          style={[{height: 32, width: 32, tintColor: fontColorPrimary}]}
          resizeMode="contain"
        />
        <View
          style={{
            alignItems: 'center',
            justifyContent: 'center',
            marginRight: 10,
            marginLeft: 5,
          }}>
          <Text
            style={{
              fontFamily: theme().root.bb_font_regular,
              fontSize: 20,
              color: fontColorPrimary,
            }}>
            {cantNotificaciones <= 0 ? '' : cantNotificaciones}
          </Text>
        </View>
      </View>
    </Pressable>
  );
}

export default NotificationBoton;
