import {View, StyleSheet} from 'react-native';

import {theme} from '../../theme/theme';

export function Boxs({children, style, padding}: any) {
  const styles = StyleSheet.create({
    boxs: {
      padding: !padding ? 16 : padding,
      color: theme().root.bb_gray,
      fontFamily: theme().root.bb_font_regular,
      backgroundColor: theme().root.bb_white,
      borderRadius: 30,

      shadowColor: '#000000',
      shadowOffset: {
        width: 0,
        height: 0,
      },
      shadowOpacity: 0.2,
      shadowRadius: 4,
      elevation: 2,
    },
  });

  return (
    <View style={style}>
      <View style={styles.boxs}>{children}</View>
    </View>
  );
}

export default Boxs;
