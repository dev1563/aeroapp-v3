import {View, StyleSheet} from 'react-native';

import {theme} from '../../theme/theme';

export function Body({children}: any) {
  return (
    <View style={style.body}>
      {children}
    </View>
  );
}

const style = StyleSheet.create({
  body: {
    backgroundColor: theme().root.bb_white,
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'flex-end',
    color: theme().root.bb_gray,
    fontFamily: theme().root.bb_font_regular,
  },
});

export default Body;
