import React, { useState, useEffect } from 'react';
import { Alert, Dimensions, Image, Linking, Pressable, StyleSheet, Text, View } from 'react-native';
import Carousel from 'react-native-reanimated-carousel';
import { theme } from '../../theme/theme';
import { getRemoteConfig } from '../../helpers/firebase/firebase';
import Loading from '../../screens/General/Loading';
import InAppBrowser from 'react-native-inappbrowser-reborn';

export const SlidePoster = () => {
  const [sliderHome, setSliderHome] = useState('');
  const [loading, setLoading] = useState(true);
  const [fontColorPrimary, setfontColorPrimary] = useState(theme().root.bb_white);


  useEffect(() => {
    const fetchRemoteConfig = async () => {
      try {
        const value = await getRemoteConfig('SliderHome');
        const response = await fetch(value);
        const json = await response.json();
        setSliderHome(json);
        const fontColor = await getRemoteConfig('fontColorSecundary');
        setfontColorPrimary(fontColor)
        setLoading(false);
      } catch (error) {
        console.error(error);
        setLoading(false);
      }
    };

    fetchRemoteConfig();
  }, []);

  const data: any = !loading ? sliderHome : {};
  const { width: windowWidth } = Dimensions.get('window');

  async function sleep(timeout: number) {
    return new Promise(resolve => setTimeout(resolve, timeout))
  }
  const openUrl = async (url: any) => {
    try {
      if (await InAppBrowser.isAvailable()) {
        const result = await InAppBrowser.open(url, {
          // iOS Properties
          dismissButtonStyle: 'cancel',
          preferredBarTintColor: fontColorPrimary,
          preferredControlTintColor: 'white',
          readerMode: false,
          animated: true,
          modalPresentationStyle: 'fullScreen',
          modalTransitionStyle: 'coverVertical',
          modalEnabled: true,
          enableBarCollapsing: false,
          // Android Properties
          showTitle: true,
          toolbarColor: fontColorPrimary,
          secondaryToolbarColor: 'black',
          navigationBarColor: 'black',
          navigationBarDividerColor: 'white',
          enableUrlBarHiding: true,
          enableDefaultShare: false,
          forceCloseOnRedirection: false,

          animations: {
            startEnter: 'slide_in_right',
            startExit: 'slide_out_left',
            endEnter: 'slide_in_left',
            endExit: 'slide_out_right'
          },
          headers: {
            'my-custom-header': 'my custom header value'
          }
        })
        await sleep(800);
      }
      else Linking.openURL(url)
    } catch (error: any) {
      Alert.alert(error.message)
    }
  };

  const styles = StyleSheet.create({
    image: {
      flex: 1,
      borderRadius: 20,
      resizeMode: 'contain'
    },
    imageContainer: {
      flex: 1,
      borderRadius: 20,
      shadowOffset: {
        width: 0,
        height: 2,
      },
      shadowOpacity: 0.25,
      shadowRadius: 3.84,

      elevation: 5,
    },
    title: {
      fontFamily: theme().root.bb_font_regular,
      fontSize: 16,
      justifyContent: 'center',
      textAlign: 'center',
      marginTop: 10,
      color: fontColorPrimary
    }
  });

  const loadingPage = () => {
    return <Loading isLoading={loading} />;
  };

  const Slide = () => {
    return (
      <Carousel
        loop
        width={windowWidth - 25}
        height={240}
        autoPlay={true}
        scrollAnimationDuration={4000}
        data={data.slide}
        renderItem={({ index }) => (
          <Pressable onPress={() => openUrl(data.slide[index].action)}>
            <View style={{ width: windowWidth - 40, height: 240 }}>
              <View style={styles.imageContainer}>
                <Image source={{ uri: data.slide[index].url }} style={styles.image} resizeMode="stretch" />
              </View>
              <Text style={styles.title}>{data.slide[index].title}</Text>
            </View>
          </Pressable>
        )}
      />
    );
  };

  return loading ? loadingPage() : Slide();
};
