import { View, Pressable, Image, StyleSheet, Text, Button } from 'react-native';
import Animated, { useSharedValue, useAnimatedStyle } from 'react-native-reanimated';
import { useSafeAreaInsets } from 'react-native-safe-area-context';
import LinearGradient from 'react-native-linear-gradient';
import NotificationBoton from '../General/NotificationBoton';
import { theme } from '../../theme/theme';
import { useEffect, useState } from 'react';
import { getRemoteConfig } from '../../helpers/firebase/firebase';
import { DrawerScreenProps } from '@react-navigation/drawer';
//import Icon_Menu from '../../../assets/icons/icon-menu.png';

interface Props extends DrawerScreenProps<any, any>{}

export function HeaderGradient({
  children,
  scrollY,
  height,
  img,
  dataimg,
  title,
  toggle,
  navigation,
  styleIndex,
  notificationsData,
}: any) {
  const { top } = useSafeAreaInsets();

  const [primaryColor, setPrimaryColor] = useState(theme().root.bb_white);
  const [fontColorPrimary, setfontColorPrimary] = useState(theme().root.bb_white);

  useEffect(() => {
    navigation.setOptions({
      headerLeft: () => (
        <Button 
        title="Menu"
        onPress={() => navigation.toggleDrawer()}/>
      )
    })
  }, [])

  getRemoteConfig('primaryColor')
    .then((value: any) => {
      setPrimaryColor(value)
    })
    .catch((error) => {
      console.log(error);
    });

    getRemoteConfig('fontColorPrimary')
    .then((value: any) => {
      setfontColorPrimary(value)
    })
    .catch((error) => {
      console.log(error);
    });

  const heightHeader = top + height || top * 1.6 + 160;
  const minHeader = top + 60;
    const animatedStyleHeader = useAnimatedStyle(() => {
    let height;
    let borderRadius;
    if (scrollY.value < 0) {
      height = scrollY.value * -1 + heightHeader;
      borderRadius = 50;
    } else if (
      scrollY.value >= 0 &&
      scrollY.value <= heightHeader - minHeader
    ) {
      height = heightHeader - scrollY.value;
      borderRadius = 50 - (25 / (heightHeader - minHeader)) * scrollY.value;
    } else {
      height = minHeader;
      borderRadius = 25;
    }
    return {
      height,
      borderBottomLeftRadius: borderRadius,
      borderBottomRightRadius: borderRadius,
    };
  });

  const imageHeightOpacity = useAnimatedStyle(() => {
    let height;
    if (scrollY.value < 0) {
      height = 150;
    } else if (
      scrollY.value >= 0 &&
      scrollY.value <= heightHeader - minHeader
    ) {
      height = 150 - (80 / (heightHeader - minHeader)) * scrollY.value;
    } else {
      height = 80;
    }
    return {
      height,
    };
  });

  const style = StyleSheet.create({
    header: {
      height: heightHeader,
      position: 'absolute',
      borderBottomLeftRadius: 50,
      borderBottomRightRadius: 50,
      overflow: 'hidden',
      top: 0,
      left: 0,
      right: 0,
    },
    gradient: { position: 'relative', flex: 1, overflow: 'hidden' },
    imagenFondo: { position: 'relative', flex: 1, overflow: 'hidden', width: '100%', height: '100%' },
    contenedorImagen: {
      opacity: 1,
      position: 'relative',
      flex: 1,
      alignItems: 'center',
      justifyContent: 'center',
    },
    imagenLogo: {
      width: '100%',
      height: 150,
      marginTop: top,
    },
  });

  return (
    <Animated.View
      style={[
        style.header,
        animatedStyleHeader,
        styleIndex ? { zIndex: styleIndex } : {},
      ]}>
      <LinearGradient
        start={{ x: 0, y: 0 }}
        end={{ x: 1, y: 0 }}
        colors={[primaryColor, primaryColor]}
        style={style.gradient}>
          
        {
          // Saber si debe mostrar el boton de ir atras
          toggle === true && (
            <Pressable
              style={{
                zIndex: 3,
                flex: 1,
                position: 'absolute',
                marginTop: 11 + top,
                marginLeft: 20
              }}
              onPress={() => {
                navigation.toggleDrawer();
              }}>
                 <Image
                  //source={Icon_Menu}
                  style={[{height: 32, width: 32, tintColor: fontColorPrimary}]}
                  resizeMode="contain"
                />
            </Pressable>
          )
        }
        {
          // Saber si debe mostrar el boton de notificaciones
          notificationsData && <NotificationBoton navigation={navigation} />
        }
        {
          // Saber si tiene texto para mostrar de titulo
          title && title !== '' && (
            <View
              style={{
                flex: 1,
                position: 'absolute',
                width: '100%',
                marginTop: 13 + top,
                alignItems: 'center',
                paddingHorizontal: 85
              }}>
              <Text
                style={{
                  fontFamily: theme().root.bb_font_bold,
                  fontSize: 24,
                  color: fontColorPrimary,
                  textAlign: 'center',
                }}>
                {title}
              </Text>
            </View>
          )
        }
        {
          // Si tiene titulo no puede mostrar imagen
          (!title || title === '') && img && (
                        
            <View style={style.contenedorImagen}>
              <Animated.Image
                source={typeof img === 'number' ? img : { uri: img }}
                style={[style.imagenLogo, imageHeightOpacity]}
                resizeMode="contain"
              />
            </View>
          )
        }
        {
          // Si tiene titulo no puede mostrar imagen
          (!title || title === '') && (!img || img === '') && dataimg && (
            <View style={style.contenedorImagen}>
              <Animated.Image
                source={dataimg != 'vacio' ? { uri: dataimg } : { uri: dataimg }}
                style={[style.imagenFondo, imageHeightOpacity]}
                resizeMode="cover"
              />
            </View>
          )
        }
      </LinearGradient>
      {children}
    </Animated.View>
  );
}

export default HeaderGradient;
