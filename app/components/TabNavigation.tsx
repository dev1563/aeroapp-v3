import React, { useState, useEffect } from 'react';
import { Dimensions, Image, StyleSheet, View } from 'react-native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import FlightListScreen from '../screens/General/ListTrips';
import Booking from '../screens/General/Booking';
import { GeneralStack } from '../navigator/GeneralStack';
import { useTranslation } from 'react-i18next';
import { getRemoteConfig } from '../helpers/firebase/firebase';
import { theme } from '../theme/theme';
import { HomeStack } from '../navigator/HomeStack';

const Tab = createBottomTabNavigator();
const { width, height } = Dimensions.get('window');

const TabNavigation = () => {
  const [primaryColor, setPrimaryColor] = useState('#ffffff');
  const [fontColorPrimary, setfontColorPrimary] = useState(theme().root.bb_white);

  const [menu, setMenu] = useState({
    icons: {
      //home: require('../assets/icons/home.png'),
      //book: require('../assets/icons/book.png'),
      //trips: require('../assets/icons/mytrips.png'),
      //acc: require('../assets/icons/user.png'),
    },
    titles: {
      tab1: 'Home',
      tab2: 'Booking',
      tab3: 'My Trips',
      tab4: 'Account',
    },
  });

  useEffect(() => {
    const fetchRemoteConfig = async () => {
      try {
        const color = await getRemoteConfig('primaryColor');
        setPrimaryColor(color);
        const menuRemote: any = await getRemoteConfig('menu');
        const menuConfig = JSON.parse(menuRemote);
        setMenu(menuConfig);
        const fontColor: any = await getRemoteConfig('fontColorSecundary');
        setfontColorPrimary(fontColor);
      }
      catch (error) {
        console.error('Error fetching remote config:', error);
      }
    };

    fetchRemoteConfig();
  }, []);



  const styles = StyleSheet.create({
    navigation: {
      borderRadius: 50,
      zIndex: 1,
      margin: '2.5%',
      paddingTop: 10,
      paddingRight: 20,
      paddingLeft: 20,
      paddingBottom: 10,
      width: '95%',
      height: 60,
      position: 'relative',
      flexDirection: 'row',
      justifyContent: 'space-between',
      shadowColor: '#000',
      shadowOffset: {
        width: 0,
        height: 4,
      },
      shadowOpacity: 0.2,
      shadowRadius: 4,
      elevation: 5,
      fontFamily: theme().root.bb_font_regular
    },
    navItem: {
      flex: 1,
      flexDirection: 'column',
      alignItems: 'center',
      color: primaryColor,
      fontSize: 10,
      marginHorizontal: 5,
      paddingBottom: 5,
      fontFamily: theme().root.bb_font_regular
    },
    activeNavItem: {
      width: 25,
      height: 25,
      marginHorizontal: 5,
      marginBottom: 5,
      tintColor: primaryColor,
    },
    navItemIcon: {
      width: 25,
      height: 25,
      marginHorizontal: 5,
      marginBottom: 5,
      tintColor: primaryColor      
    },
  });

  const { t } = useTranslation();

  return (
    <View style={{ width, height, paddingBottom: 25, backgroundColor: 'transparent' }}>
      <Tab.Navigator
        screenOptions={{
          tabBarStyle: styles.navigation,
          headerShown: false,
          tabBarActiveTintColor: primaryColor,
          tabBarHideOnKeyboard: true,
        }}
        sceneContainerStyle={{
          backgroundColor: '#FFF',
          opacity: 1,
        }}>
        <Tab.Screen
          name={menu.titles.tab1}
          component={HomeStack}
          options={{
            tabBarIcon: ({ focused }) => (
              <Image 
              //source={menu.icons.home} 
              style={focused ? styles.activeNavItem : styles.navItemIcon} />
            ),
          }}
        />
        <Tab.Screen
          name={menu.titles.tab2}
          component={Booking}
          options={{
            tabBarIcon: ({ focused }) => (
              <Image 
              //source={menu.icons.book} 
              style={focused ? styles.activeNavItem : styles.navItemIcon} />
            ),
          }}
        />
        <Tab.Screen
          name={menu.titles.tab3}
          component={GeneralStack}
          options={{
            tabBarIcon: ({ focused }) => (
              <Image 
              //source={menu.icons.trips} 
              style={focused ? styles.activeNavItem : styles.navItemIcon} />
            ),
          }}
        />
        <Tab.Screen
          name={menu.titles.tab4}
          component={FlightListScreen}
          options={{
            tabBarIcon: ({ focused }) => (
              <Image 
              //source={menu.icons.acc} 
              style={focused ? styles.activeNavItem : styles.navItemIcon} />
            ),
          }}
        />
      </Tab.Navigator>
    </View>
  );
};

export default TabNavigation;
