import { Dimensions, StyleSheet } from 'react-native';
import { useSafeAreaInsets } from 'react-native-safe-area-context';
import { theme } from './theme';

const {top, bottom} = useSafeAreaInsets();
const windowHeight = Dimensions.get('window').height;

export const Loginstyles = StyleSheet.create({
    formContainer: {
        flex: 1,
        paddingHorizontal: 30,
        justifyContent: 'center',
        height: 600,
        marginBottom: 50
    },
    contenedorLogin: {
      flex: 1,
      paddingTop: top * 1.5 + 160 + 16,
      alignItems: 'center',
      justifyContent: 'center',
      padding: 16,
      minHeight: windowHeight * 0.75 - bottom,
    },
    contenedorRegister: {
        flex: 1,
        paddingTop: top * 1.5 + 160 + 16,
        alignItems: 'center',
        justifyContent: 'center',
        padding: 16,
        minHeight: windowHeight * 0.75 - bottom,
    },
    title: {
        color: 'white',
        fontSize: 30,
        fontWeight: 'bold',
        marginTop: 20
    },
    tituloLogin: {
        color: theme().root.bb_dark,
        fontSize: 16,
        marginTop: 16,
        marginBottom: 20,
    },
      tituloRegister: {
        color: theme().root.bb_dark,
        fontSize: 16,
        marginTop: 16,
        marginBottom: 20,
    },
    contenedorCampo: {
        width: '100%',
        height: 58,
        backgroundColor: theme().root.bb_white,
        borderRadius: 50,
        justifyContent: 'center',
        marginBottom: 16,
        shadowColor: '#000000',
        shadowOffset: {
          width: 0,
          height: 0,
        },
        shadowOpacity: 0.3,
        shadowRadius: 5,
        elevation: 5,
    },
    roundImagen: {
        width: 42,
        height: 42,
        backgroundColor: theme().root.bb_purple,
        borderRadius: 50,
        position: 'absolute',
        marginLeft: 10,
        justifyContent: 'center',
        alignItems: 'center',
    },
    imageRound: {
        width: 26, 
        height: 26,
        tintColor: theme().root.bb_light
    },
    titulocampo: {
        position: 'absolute',
        marginLeft: 62,
        height: 26,
        fontSize: 18,
        color: theme().root.bb_dark,
    },
    titulocampoFocus: {
        position: 'absolute',
        marginLeft: 62,
        height: 43,
        fontSize: 13,
        color: theme().root.bb_dark,
    },
    label: {
        marginTop: 25,
        color: 'white',
        fontWeight: 'bold'
    },
    campoTexto: {
        zIndex: 1,
        paddingLeft: 62,
        paddingRight: 16,
        //paddingTop: Platform.OS === 'ios' ? 18 : 29,
        marginTop: 18,
        color: theme().root.bb_dark,
        fontSize: 18,
        width: '100%',
        height: 58,
    },
    campoTextoPassword: {
        zIndex: 1,
        paddingLeft: 62,
        paddingRight: 16,
        //paddingTop: Platform.OS === 'ios' ? 18 : 29,
        marginTop: 18,
        color: theme().root.bb_dark,
        fontSize: 18,
        width: '83%',
        height: 58,
    },
    inputField: {
        color: 'white',
        fontSize: 20
    },
    inputFieldIOS: {
        borderBottomColor: 'white',
        borderBottomWidth: 2,
        paddingBottom: 4
    },
    contenedorMostrarContrasena: {
        width: '100%',
        flex: 1,
        justifyContent: 'center',
        alignItems: 'flex-end',
        position: 'absolute',
    },
    btnMostrarContrasena: {
        width: 42,
        height: 42,
        marginRight: 10,
        justifyContent: 'center',
        alignItems: 'center',
    },
    imageRoundContrasena: {
        width: 24, 
        height: 24,
        tintColor: theme().root.bb_orange,
    },
    buttonContainer: {
        alignItems: 'center',
        marginTop: 50
    },
    button: {
        borderWidth: 2,
        borderColor: 'white',
        paddingHorizontal: 20,
        paddingVertical: 5,
        borderRadius: 100
    },
    buttonText: {
        fontSize: 18,
        color: 'white'
    },
    newUserContainer: {
        alignItems: 'flex-end',
        marginTop:10
    },
    btnOlvidoClave: {
        marginTop: 15,
        marginBottom: 15,
        height: 25,
        marginLeft: 200,
    },
    txtolvidocontrasena: {
        color: theme().root.bb_orange,
        fontSize: 16,
    },
    btnlogin: {
        marginTop: 7,
        width: '100%',
    },
    btnRegister: {
        marginTop: 7,
        width: '100%',
    },
    buttonReturnLogin: {
        position: 'absolute',
        top: 50,
        left: 20,
        borderWidth: 1,
        borderColor: 'white',
        paddingHorizontal: 10,
        paddingVertical: 5,
        borderRadius: 100,
    },
    contenedorSignup: {
        flex: 1,
        alignItems: 'center',
        padding: 16,
        minHeight: windowHeight * 0.25 + bottom,
        paddingBottom: bottom,
    },
    contenedorReturnLogin: {
        flex: 1,
        alignItems: 'center',
        padding: 16,
        minHeight: windowHeight * 0.25 + bottom,
        paddingBottom: bottom,
    },
    hrlinea: {
        borderTopWidth: 1,
        width: '100%',
        borderColor: theme().hrcolor,
        height: 1,
    },
    txtnotaccount: {
        color: theme().root.bb_dark,
        fontSize: 14,
        marginTop: 16,
    },
    txtreadyaccount: {
        color: theme().root.bb_dark,
        fontSize: 14,
        marginTop: 16,
    },
    btnRegistro: {
        marginTop: 16, 
        width: 180,
    },
    btnReturnLogin: {
        marginTop: 16, 
        width: 180,
    },
});