import {StyleSheet} from 'react-native';

//import Icon_Alert from '../assets/img/icons/icon-alert.png';

const theme = () => {
  //const icons = {Icon_Alert};

  const root = {
    bb_orange: '#e7541c',
    bb_purple: '#3c2665',
    bb_red: '#f40009',
    bb_gray: '#f6f6f6',
    bb_light: '#f6f7f8',
    bb_white: '#ffffff',
    bb_dark: '#444444',
    bb_font_regular: 'Poppins-Regular',
    bb_font_bold: 'Poppins-Bold',
    bb_font_bold_italic: 'Poppins-BoldItalic',
    bb_font_italic: 'Poppins-Italic',
    bb_font_light: 'Poppins-Light',
    bb_font_light_italic: 'Poppins-LightItalic',
  };

  const hrcolor = '#d0d5dc';

  const alertas = StyleSheet.create({
    contenedorAlerta: {
      flexDirection: 'row',
      alignItems: 'center',
      marginTop: 25,
    },
    imagenAlerta: {height: 25, width: 25, marginRight: 7},
    textoAlerta: {
      fontFamily: root.bb_font_regular,
      fontSize: 17,
      color: root.bb_red,
      marginTop: 2,
    },
  });

  return {
    //icons,
    root,
    hrcolor,
    alertas,
  };
};

export {theme};
