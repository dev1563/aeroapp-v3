import { Dimensions, StyleSheet } from 'react-native';
import { useSafeAreaInsets } from 'react-native-safe-area-context';
import { theme } from './theme';

const {top, bottom} = useSafeAreaInsets();
const windowHeight = Dimensions.get('window').height;

export const Homestyles = StyleSheet.create({
    contenedorHeader: {
        zIndex: 1,
    },
    contenedorBoxi: {
        zIndex: 1,
        paddingHorizontal: 16,
        marginTop: top + 140,
        position: 'absolute',
        width: '100%',
    },
    scrollContenedor: {
        marginTop: top + 120,
        paddingHorizontal: 16,
        paddingVertical: 20,
    },
    contenedorCategories: {
        flex: 1,
        width: '100%',
    },
    titleContenedorCategories: {
        display: 'flex',
        alignContent: 'flex-start',
        marginBottom: 10,
    },
    titleCategories: {
        width: '100%',
        fontFamily: theme().root.bb_font_bold,
        color: theme().root.bb_purple,
        fontSize: 18,
        marginRight: 220,
    },
    contenedorCardCategories: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginBottom: 20,
        width: '100%'
    },
    spanCategories: {
        width: 60,
        padding: 20,
        lineHeight: 60,
        height: 60,
        backgroundColor: theme().root.bb_purple,
        borderRadius: 6,
        display: 'flex',
    },
    iconCategories: {
        width: 30,
        height: 30,
        tintColor: theme().root.bb_white,
    },
    subtitleCategories: {
        position: 'absolute',
        width: 90,
        //textAlign: 'center',
        color: theme().root.bb_dark,
        fontSize: 11,
        fontFamily: theme().root.bb_font_regular,
        //padding: 5,
        marginTop: 70,
        paddingHorizontal: 10
    },
    cardHome: {
        backgroundColor: theme().root.bb_white,
        padding: 15,
        width: 200,
        height: 200,
        borderRadius: 10,
        shadowColor: '#000000',
        shadowOffset: {
          width: 0,
          height: 0,
        },
        shadowOpacity: 0.3,
        shadowRadius: 5,
        elevation: 5,
        marginHorizontal: 5,
    },
    imgVideoHome: {
        width: 170,
        height: 130,
        marginBottom: 5,
        borderTopLeftRadius: 7,
        borderTopRightRadius: 7
    },
    cardHome2: {
        backgroundColor: theme().root.bb_white,
        padding: 15,
        width: 150,
        height: 170,
        borderRadius: 10,
        shadowColor: '#000000',
        shadowOffset: {
          width: 0,
          height: 0,
        },
        shadowOpacity: 0.3,
        shadowRadius: 5,
        elevation: 5,
        marginHorizontal: 10
    },
    imgVideoHome2: {
        width: 90,
        height: 120,
        marginRight: 10,
        marginBottom: -10,
        marginTop: -10,
        borderTopLeftRadius: 7,
        borderTopRightRadius: 7
    },
    btnlogin: {
        marginTop: 7,
        width: '100%',
    },
    contenedorCampo: {
        width: '95%',
        height: 58,
        backgroundColor: theme().root.bb_white,
        borderRadius: 50,
        justifyContent: 'center',
        marginBottom: 16,
        shadowColor: '#000000',
        shadowOffset: {
          width: 0,
          height: 0,
        },
        shadowOpacity: 0.3,
        shadowRadius: 5,
        elevation: 5,
        marginLeft: 10,
    },
    roundImagen: {
        width: 42,
        height: 42,
        backgroundColor: theme().root.bb_purple,
        borderRadius: 50,
        position: 'absolute',
        marginLeft: 10,
        right: 10,
        justifyContent: 'center',
        alignItems: 'center',
    },
    imageRound: {
        width: 26, 
        height: 26,
        tintColor: theme().root.bb_light
    },
    titulocampo: {
        position: 'absolute',
        marginLeft: 32,
        height: 26,
        fontSize: 15,
        color: theme().root.bb_dark,
        fontFamily: theme().root.bb_font_regular,
    },
    titulocampoFocus: {
        position: 'absolute',
        marginLeft: 32,
        height: 43,
        fontSize: 10,
        fontFamily: theme().root.bb_font_regular,
    },
    label: {
        marginTop: 25,
        color: 'white',
        fontWeight: 'bold'
    },
    campoTexto: {
        zIndex: 1,
        paddingLeft: 32,
        paddingRight: 16,
        //paddingTop: Platform.OS === 'ios' ? 18 : 29,
        marginTop: 18,
        color: theme().root.bb_dark,
        fontFamily: theme().root.bb_font_regular,
        fontSize: 18,
        width: '100%',
        height: 58,
    },
    contenedorCampoHeader: {
        marginTop: 60, 
        marginLeft: 30,
    },
    roundImagenHeader: {
        width: 50,
        height: 50,
        backgroundColor: theme().root.bb_white,
        borderRadius: 50,
        position: 'absolute',
        marginLeft: 10,
        justifyContent: 'center',
        alignItems: 'center',
    },
    imageRoundHeader: {
        width: 40, 
        height: 40,
        borderRadius: 50,
    },
    titulocampoHeader: {
        position: 'absolute',
        marginLeft: 70,
        height: 26,
        fontSize: 18,
        fontFamily: theme().root.bb_font_bold,
    },
    titulocampoFocusHeader: {
        position: 'absolute',
        marginLeft: 70,
        height: 26,
        marginTop: 20,
        fontSize: 16,
        fontFamily: theme().root.bb_font_regular,
    },
});
