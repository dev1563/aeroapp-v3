export interface LoginData {
    correo: string;
    password: string;
}

export interface RegisterData {
    correo: string;
    password: string;
    nombre: string;
}

export interface LoginResponse {
    usuario: Usuario;
    token: string;
}

export interface Usuario {
    rol: string;
    estado: boolean;
    google: boolean;
    nombre: string;
    correo: string;
    uid: string;
    img?: string;
}

export interface AuthState {
    status: 'checking' | 'authenticated' | 'not-authenticated';
    token: string | null;
    errorMessage: string;
    user: Usuario | null;
}

export interface ImgSlide {
    slide: slide;
}

export interface slide {
    title: string;
    text: string;
    url: string;
}

export interface PosterData {
    name:  string;
    items: Item[];
}

export interface Item {
    img:          string;
    titleKey:     string;
    route:        string;
}

export interface SectionVisible {
    Slide:        boolean;
    Categories:   boolean;
    Destinations: boolean;
    Offers:       boolean;
}
