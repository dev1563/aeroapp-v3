import React, { createContext, useEffect, useReducer } from 'react';
import AsyncStorage from '@react-native-async-storage/async-storage';
import auth from '@react-native-firebase/auth';
import { AuthState, LoginData, RegisterData } from '../../interfaces/appInterfaces';
import { AuthContextProps } from '../../types/TAuthContextProps';
import { AuthReducer } from '../AuthContext/AuthReducer';


const AuthInitialState: AuthState = {
    status: 'checking',
    token: null,
    user: null,
    errorMessage: ''
}

export const AuthContext = createContext( {} as AuthContextProps );


export const AuthProvider = ({ children }: any ) => {
    
    const [ state, dispatch ] =  useReducer( AuthReducer, AuthInitialState );
    
    // Handle user state changes
    function onAuthStateChanged(user: any) {
        if ( !user ){
            return dispatch({ type: 'notAuthenticated' });
        }else{
            return dispatch({ 
                type: 'signUp',
                payload: {
                    token: user.email,
                    user: user.email
                }
            });
        }
    }
    
    // Listening to authentication state
    useEffect(() => {
        const subscriber = auth().onAuthStateChanged(onAuthStateChanged);
        return subscriber; // unsubscribe on unmount
    }, []);

    /*useEffect(() => {
        checkToken();
    }, [])

    const checkToken = async() => {
        const token = await AsyncStorage.getItem( 'token' );
        
        if ( !token ) return dispatch({ type: 'notAuthenticated' });

        const resp = await UsersApi.get( endpoints.doCheck  );
        if ( resp.status !== 200 ){
            return dispatch({ type: 'notAuthenticated' });
        }

        await AsyncStorage.setItem( 'token', resp.data.token );

        dispatch({ 
            type: 'signUp',
            payload: {
                token: resp.data.token,
                user: resp.data.usuario
            }
        });

    }*/

    const signIn = async( {correo, password}: LoginData ) => {
        try {

            if(correo !== '' && password !== ''){ 
                //const { data } = await UsersApi.post<LoginResponse>( endpoints.doLogin, { correo, password })
                const { user }:any = await auth().signInWithEmailAndPassword(correo, password).then(() => {
                    const  data: any  = {correo, password}
                    dispatch({ 
                        type: 'signUp',
                        payload: {
                            token: user.uid,
                            user: data.usuario
                        }
                    });
                }).catch(error => {
                    if (error.code === 'auth/email-already-in-use') {
                        dispatch({ 
                            type: 'addError', 
                            payload: 'That email address is already in use!'
                        })
                    }
                
                    if (error.code === 'auth/invalid-email') {
                        dispatch({ 
                            type: 'addError', 
                            payload: 'That email address is invalid!'
                        })
                    }
                });
                
                await AsyncStorage.setItem( 'token', user.uid );
            } else {
                dispatch({ 
                    type: 'addError', 
                    payload: 'Información incorrecta'
                })
            }

        } catch (error: any) {

            dispatch({ 
                type: 'addError', 
                payload: error.response.data.msg || 'Información incorrecta'
            })

        }
    };

    const signUp = async( {nombre, correo, password }: RegisterData ) => {

        try {

            if(correo !== '' && password !== '' && nombre !== ''){ 
                //const { data } = await UsersApi.post<LoginResponse>( endpoints.doRegister, { correo, password, nombre })
                const { user }: any = await auth().createUserWithEmailAndPassword(correo, password).then(() => {
                    const  data: any  = {correo, password, nombre}
                    dispatch({ 
                        type: 'signUp',
                        payload: {
                            token: user.uid,
                            user: data.usuario
                        }
                    });
                }).catch(error => {
                    if (error.code === 'auth/email-already-in-use') {
                        dispatch({ 
                            type: 'addError', 
                            payload: 'That email address is already in use!'
                        })
                    }
                
                    if (error.code === 'auth/invalid-email') {
                        dispatch({ 
                            type: 'addError', 
                            payload: 'That email address is invalid!'
                        })
                    }
                });
                
                await AsyncStorage.setItem( 'token', user.uid );
            }else {
                dispatch({ 
                    type: 'addError', 
                    payload: 'Información incorrecta'
                })
            }

        } catch (error: any) {

            dispatch({ 
                type: 'addError', 
                payload: error.response.data.errors[0].msg || 'Revise la información'
            })

        }

    };

    const logOut = async() => {
        await AsyncStorage.removeItem( 'token' );
        await auth().signOut();
        dispatch({ type: 'logOut' });
    };

    const removeError = () => {
        dispatch({ type: 'removeError'});
    };

    return (
        <AuthContext.Provider value={{
            ...state,
            signUp,
            signIn,
            logOut,
            removeError,
        }}>
            { children }
        </AuthContext.Provider>
    )
}