import React, { useState } from 'react';
import { View, Text, StyleSheet, TextInput, ScrollView } from 'react-native';
import { useSharedValue } from 'react-native-reanimated';
import { StackScreenProps } from '@react-navigation/stack';
import Body from '../../components/General/Body';
import HeaderGradient from '../../components/General/HeaderGradient';
import { theme } from '../../theme/theme';
import Boton from '../../components/General/Boton';
import { useTranslation } from 'react-i18next';

interface Props extends StackScreenProps<any, any> {}

const MyTrips = ({ navigation }: Props) => {
  const [reserva, setReserva] = useState('');
  const [apellido, setApellido] = useState('');
  const [loading, setLoading] = useState(false);

  const scrollY = useSharedValue(0);
  const { t } = useTranslation();

  const handleCheckin = () => {
    navigation.navigate('ListTrips')
  };

  const styles = StyleSheet.create({
    innerWrapper: {
      marginTop: 35,
      paddingHorizontal: 16,
      zIndex: 1,
    },
    subTitle: {
      marginTop: 35,
      fontSize: 16,
      color: theme().root.bb_white,
      textAlign: 'center',
    },
    subText: {
      fontFamily: theme().root.bb_font_regular,
      fontSize: 14,
      textAlign: 'center',
      color: theme().root.bb_white,
    },
    formContainer: {
      padding: 16,
    },
    input: {
      fontFamily: theme().root.bb_font_regular,
      backgroundColor: '#fff',
      borderRadius: 50,
      shadowColor: '#000',
      shadowOpacity: 0.2,
      shadowOffset: { width: 0, height: 2 },
      shadowRadius: 4,
      elevation: 2,
      fontSize: 16,
      color: '#444444',
      paddingLeft: 20,
      height: 50,
      marginBottom: 10,
    },
    textCenter: {
      width: '95%',
      alignItems: 'center',
      justifyContent: 'center',
      paddingLeft: 20,
      },
    scrollContainer: {
      flex: 1,
      marginTop: 15, // Ajusta la separación superior si es necesario
      marginBottom: 0, // Ajusta la separación inferior si es necesario
    },
  });

  return (
    <Body>
      <>
        <HeaderGradient
          scrollY={scrollY}
          navigation={navigation}
          title={t('tripsScreen.txtTitle')}
          height={180}>
        </HeaderGradient>
        <View style={{ zIndex: 1 }}>
          <View style={styles.innerWrapper}>
            <Text style={styles.subTitle}></Text>
            <Text style={styles.subText}></Text>
          </View>
        </View>
        <ScrollView style={styles.scrollContainer}>
          <View style={styles.formContainer}>
            <TextInput
              style={styles.input}
              placeholder={t('tripsScreen.inputCode')}
            />
            <TextInput
              style={styles.input}
              placeholder={t('tripsScreen.inputLastName')}
            />
          </View>
          <View style={styles.textCenter}>
            <Boton
              nombreBtn={t('tripsScreen.btnReservation')}
              cargando={loading}
              onPress={handleCheckin}>
            </Boton>
          </View>
        </ScrollView>
      </>
    </Body >
  );
};

export default MyTrips;
