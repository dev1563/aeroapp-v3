import React, { Dispatch, SetStateAction, useState } from 'react';
import { View } from 'react-native';
import Lottie from 'lottie-react-native';
import { theme } from '../../theme/theme';
import { getRemoteConfig } from '../../helpers/firebase/firebase';
import Loading from './Loading';


interface SplashProps {
  setIsLoading: Dispatch<SetStateAction<boolean>>;
}

const Splash = ({ setIsLoading }: SplashProps) => {
  const [splashLottie, setsplashLottie] = useState('../../assets/splashbb.json');
  const [loading, setLoading] = useState(false);

  getRemoteConfig('splashLottie')
    .then((value: any) => {
      setsplashLottie(value);
      setLoading(false);
    })
    .catch((error) => {
      setLoading(true)
      console.log(error);
    });

  const [primaryColor, setPrimaryColor] = useState(theme().root.bb_white);
  getRemoteConfig('primaryColor')
    .then((value: any) => {
      setPrimaryColor(value)
      setLoading(false);
    })
    .catch((error) => {
      setLoading(true)
      console.log(error);

    });

  const loadingPage = () => {
    return (
      <Loading isLoading={loading} ></Loading>
    )
  }

  const SplashPage = () => {
    return (
      <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', backgroundColor: primaryColor }}>
        <Lottie
          style={{ width: '100%', maxHeight: 250 }}
          source={{ uri: splashLottie }}
          autoPlay
          loop={false}
          resizeMode='contain'
          onAnimationFinish={() => setIsLoading(false)}
        />
      </View>
    )
  }
  return (
    loading ? loadingPage() : SplashPage()
  )

}

export default Splash;