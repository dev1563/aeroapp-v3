import React, { useState } from 'react';
import { View, Text, TouchableOpacity, StyleSheet, Image, ScrollView } from 'react-native';
import { useSharedValue } from 'react-native-reanimated';
import { StackScreenProps } from '@react-navigation/stack';
import Body from '../../components/General/Body';
import HeaderGradient from '../../components/General/HeaderGradient';
import { useTranslation } from 'react-i18next';
import { getRemoteConfig } from '../../helpers/firebase/firebase';
import { theme } from '../../theme/theme';
//import Icon_Plane from '../../assets/icons/icon-plane.png';
import Boton from '../../components/General/Boton';


interface Props extends StackScreenProps<any, any> { }

const FlightListScreen = ({ navigation }: Props) => {
  const scrollY = useSharedValue(0);
  const { t } = useTranslation();
  const [loading, setLoading] = useState(false);

  const [fontColorPrimary, setfontColorPrimary] = useState(theme().root.bb_white);
  getRemoteConfig('fontColorSecundary')
    .then((value: any) => {
      setfontColorPrimary(value)
    })
    .catch((error) => {
      console.log(error);      
    });
    
  const MyTripsNative = () => {
    return (
      <>
        <HeaderGradient
          scrollY={scrollY}
          navigation={navigation}
          title={t('tripsScreen.listTripsScreen.txtTitle')}
          height={180}>
        </HeaderGradient>
        <View style={{ zIndex: 1 }}>
          <View style={styles.innerWrapper}>
            <Text style={styles.subTitle}>{t('tripsScreen.listTripsScreen.txtSubTitle')}</Text>
          </View>
          <View style={styles.tabMenu}>
            <TouchableOpacity style={[styles.tabButton, styles.activeTab]}>
              <Text style={styles.tabButtonText}>{t('tripsScreen.listTripsScreen.txtUpcoming')}</Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.tabButton}>
              <Text style={styles.tabButtonText}>{t('tripsScreen.listTripsScreen.txtArchived')}</Text>
            </TouchableOpacity>
          </View>
        </View>
        <ScrollView style={styles.scrollContainer}>
          <View style={styles.boxes}>
            <View style={styles.cardHeader}>
              <View style={styles.headerRow}>
                <View style={styles.two}>
                  <Text style={styles.reservationCodeText}>{t('tripsScreen.listTripsScreen.txtReservationCode')} <Text style={styles.reservationCode}>BEB3GB00</Text></Text>
                </View>
                <View style={styles.two}>
                  <Text style={styles.dateText}>05 Feb 2024{'\n'}<Text style={styles.timeText}>10:00am</Text></Text>
                </View>
              </View>
            </View>
            <View style={styles.destinationContainer}>
              {/* Origin */}
              <View style={styles.origin}>
                <Text style={styles.originCode}>BOG</Text>
                <Text style={styles.originText}>Bogotá</Text>
                <Text style={styles.originAirport}>El Dorado</Text>
              </View>
              {/* Airplane Icon */}
              <View style={styles.airplaneContainer}>
                <Image 
                //source={Icon_Plane} 
                style={styles.airplaneIcon} />
              </View>
              {/* Destination */}
              <View style={styles.destination}>
                <Text style={styles.destinationCode}>MED</Text>
                <Text style={styles.destinationText}>Medellín</Text>
                <Text style={styles.destinationAirport}>Olaya Herrera</Text>
              </View>
            </View>
            <View style={styles.cardFooter}>
              <View style={styles.headerRow}>
                <View style={styles.two}>
                  <Text style={styles.priceText}>P57270</Text>
                  <Text style={styles.airlineText}>be bolder Airlines</Text>
                </View>
                <View style={styles.two}>
                  <Text style={styles.flightTypeText}>Nonstop</Text>
                  <Text style={styles.ticketTypeText}>{t('tripsScreen.listTripsScreen.txtEconomy')}</Text>
                </View>
              </View>
              <View style={styles.checkIn}>
                <Text style={styles.checkInText}>BOG {'>'} MED</Text>
                <Text style={styles.checkInTextMiddle}>Check-in</Text>
                <Text style={styles.checkInText}>{t('tripsScreen.listTripsScreen.txtOpened')}</Text>
              </View>
            </View>
          </View>
          <View style={styles.textCenter}>
            <View style={styles.textCenter}>
              <Boton
                nombreBtn={t('tripsScreen.listTripsScreen.btnAnotherFlight')}
                cargando={loading}>
              </Boton>
              {/* <TouchableOpacity style={styles.button}>
                <Text style={styles.buttonText}>{t('tripsScreen.listTripsScreen.btnAnotherFlight')}</Text>
              </TouchableOpacity> */}
            </View>
          </View>
        </ScrollView>
      </>
    )
  }

  const styles = StyleSheet.create({
    innerWrapper: {
      marginTop: 60,
      paddingHorizontal: 16,
      zIndex: 1,
    },
    subTitle: {
      marginTop: 35,
      fontSize: 16,
      textAlign: 'center',
      color: theme().root.bb_white,

    },
    tabMenu: {
      paddingTop: 10,
      flexDirection: 'row',
      justifyContent: 'space-around',
      marginTop: 5,
    },
    tabButton: {
      paddingHorizontal: 20,
      paddingVertical: 8,
      borderRadius: 5,
    },
    activeTab: {
      borderBottomColor: theme().root.bb_purple,
      borderBottomWidth: 4,
      fontSize: 10,
      color: theme().root.bb_white,
      fontFamily: theme().root.bb_font_bold,
    },
    tabButtonText: {
      fontSize: 16,
      color: theme().root.bb_white,
      fontFamily: theme().root.bb_font_bold,
    },

    buttonChange: {
      width: 55,
      borderRadius: 10,
      backgroundColor: theme().root.bb_purple,
      paddingVertical: 14,
      alignItems: 'center',
      justifyContent: 'center',
      marginBottom: 10,
      height: 45
    },
    button: {
      width: 340,
      backgroundColor: '#e7541c',
      borderRadius: 50,
      paddingVertical: 14,
      alignItems: 'center',
      justifyContent: 'center',
      marginBottom: 10
    },

    scrollContainer: {
      flex: 1,
      marginTop: 15,
      marginBottom: 0,
    },
    boxes: {
      backgroundColor: 'white',
      borderRadius: 20,
      zIndex: -1,
      margin: '2.5%',
      paddingVertical: 15,
      paddingHorizontal: 20,
      width: '95%',
      height: '80%',
      shadowColor: '#000',
      shadowOffset: {
        width: 0,
        height: 0,
      },
      shadowOpacity: 0.2,
      shadowRadius: 5,
      elevation: 4,
    },
    cardHeader: {
      marginBottom: 10,
      borderBottomColor: '#dddddd',
      borderBottomWidth: 1,
      paddingBottom: 12
    },
    headerRow: {
      flexDirection: 'row',
    },
    two: {
      flex: 2,
    },
    reservationCodeText: {
      color: fontColorPrimary,
      fontFamily: theme().root.bb_font_regular,
      fontSize: 16,
    },
    reservationCode: {
      fontFamily: theme().root.bb_font_bold,
    },
    dateText: {
      fontFamily: theme().root.bb_font_bold,
      color: '#444',
      fontSize: 16,
      textAlign: 'right',

    },
    timeText: {
      fontSize: 12,
    },
    destinationContainer: {
      flexDirection: 'row',
      justifyContent: 'space-between',
      borderBottomColor: '#dddddd',
      borderBottomWidth: 1,
      paddingBottom: 5
    },
    origin: {
      flex: 2,
      alignItems: 'flex-start',
    },
    originCode: {
      fontFamily: theme().root.bb_font_bold,
      fontSize: 35,
      color: fontColorPrimary,
    },
    originText: {
      color: '#333',
      fontFamily: theme().root.bb_font_regular,
      fontSize: 16,
    },
    originAirport: {
      fontFamily: theme().root.bb_font_regular,
      fontSize: 12,
    },
    airplaneContainer: {
      flex: 1,
      marginBottom: 30,
      paddingLeft: 50,
      paddingRight: 50,
      alignItems: 'center',
      borderBottomColor: theme().root.bb_purple,
      borderBottomWidth: 5,
      borderStyle: 'dotted',

    },
    airplaneIcon: {
      width: 65,
      height: 65,
      tintColor: fontColorPrimary,
    },
    destination: {
      flex: 2,
      alignItems: 'flex-end',
    },
    destinationCode: {
      fontFamily: theme().root.bb_font_bold,
      fontSize: 35,
      color: fontColorPrimary,
    },
    destinationText: {
      fontFamily: theme().root.bb_font_regular,
      color: '#333',
      fontSize: 16,
    },
    destinationAirport: {
      fontFamily: theme().root.bb_font_regular,
      fontSize: 12,
    },
    cardFooter: {
      flex: 1,
      justifyContent: 'space-between',
      marginTop: 15,
    },
    priceText: {
      fontFamily: theme().root.bb_font_bold,
      textAlign: 'left',
      fontSize: 19,
    },
    airlineText: {
      fontFamily: theme().root.bb_font_regular,
      textAlign: 'left',
      fontSize: 14,
    },
    flightTypeText: {
      fontFamily: theme().root.bb_font_bold,
      textAlign: 'right',
      fontSize: 19,
    },
    ticketTypeText: {
      fontFamily: theme().root.bb_font_regular,
      textAlign: 'right',
      fontSize: 14,
    },
    checkIn: {
      flexDirection: 'row', // Mostrar los elementos en una fila
      alignItems: 'flex-start', // Alinear los elementos verticalmente al centro
      justifyContent: 'flex-start',

    },
    checkInText: {
      fontFamily: theme().root.bb_font_bold,
      fontSize: 16,
      color: fontColorPrimary,
      fontWeight: 'bold'

    },
    checkInTextMiddle: {
      fontFamily: theme().root.bb_font_regular,
      fontSize: 16,
      color: theme().root.bb_dark,
      paddingLeft: 10,
      paddingRight: 10

    },
    buttonText: {
      fontFamily: theme().root.bb_font_bold,
      fontSize: 16,
      color: '#fff',
      fontWeight: 'bold',
    },
    textCenter: {
      alignItems: 'center',
    },
  });


  return (
    <Body>
      {MyTripsNative()}
    </Body>
  );
};

export default FlightListScreen;
