import React, { useEffect, useState } from 'react';
import { View, Text, TouchableOpacity, StyleSheet, TextInput, Image, ScrollView } from 'react-native';
import { useSharedValue } from 'react-native-reanimated';
import { StackScreenProps } from '@react-navigation/stack';
import Body from '../../components/General/Body';
import HeaderGradient from '../../components/General/HeaderGradient';
//import Icon_arrows from '../../assets/icons/icon-arrows.png';
//import Icon_Currency from '../../assets/icons/icon-currency.png';
//import Icon_PinMap from '../../assets/icons/icon-pinmap.png';
import Boxs from '../../components/General/Boxs';
import { SafeAreaView } from 'react-native-safe-area-context';
import { useTranslation } from 'react-i18next';
import { getRemoteConfig } from '../../helpers/firebase/firebase';
import { theme } from '../../theme/theme';
import WebView from 'react-native-webview';
//import Img_Example from '../../assets/img/example.png';
import remoteConfig from '@react-native-firebase/remote-config';


interface Props extends StackScreenProps<any, any> { }

const Booking = ({ navigation }: Props) => {
  const scrollY = useSharedValue(0);
  const { t } = useTranslation();

  const [booking, setBooking] = useState<boolean>();
  const [bookingUrl, setBookingUrl] = useState('');
  const [fontColorPrimary, setfontColorPrimary] = useState(theme().root.bb_white);


  useEffect(() => {
    const fetchRemoteConfig = async () => {
      try {
        //const active: any = await getRemoteConfig('activar_booking');
        const active = remoteConfig().getValue('activar_booking').asBoolean();
        setBooking(active);
        const url: any = await getRemoteConfig('booking');
        setBookingUrl(url);
        const font: any = await getRemoteConfig('fontColorPrimary');
        setfontColorPrimary(font)
      } catch (error) {
        console.error('Error fetching remote config:', error);
      }
    };

    fetchRemoteConfig();
  }, []);


  const destinations = [
    {
      city: 'New York',
      description: 'The city that never sleeps.',
      initials: 'NYC',
    },
    {
      city: 'Paris',
      description: 'The city of love and lights.',
      initials: 'PAR',
    },
    {
      city: 'Tokyo',
      description: 'A vibrant metropolis with rich traditions.',
      initials: 'TKY',
    },
    {
      city: 'London',
      description: 'Historic and modern blend seamlessly.',
      initials: 'LON',
    },
    {
      city: 'Rome',
      description: 'Eternal city of history and culture.',
      initials: 'ROM',
    },
    {
      city: 'Sydney',
      description: 'Stunning beaches and iconic landmarks.',
      initials: 'SYD',
    },
    {
      city: 'Los Angeles',
      description: 'Entertainment capital of the world.',
      initials: 'LAX',
    },
    {
      city: 'Beijing',
      description: 'Ancient and dynamic metropolis.',
      initials: 'PEK',
    },
    // ... Agrega más destinos si es necesario
  ];

  const ReturnBookingNative = () => {
    return (
      <>
        <HeaderGradient
          scrollY={scrollY}
          navigation={navigation}
          title={t('bookingScreen.txtTitle')}
          height={180}>
        </HeaderGradient>
        <View style={{ zIndex: 1 }}>
          <View style={styles.innerWrapper}>
            <Text style={styles.subTitle}>{t('bookingScreen.txtSubTitle')}</Text>
          </View>
          <View style={styles.tabMenu}>
            <TouchableOpacity style={[styles.tabButton, styles.activeTab]}>
              <Text style={styles.tabButtonText}>{t('bookingScreen.txtRT')}</Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.tabButton}>
              <Text style={styles.tabButtonText}>{t('bookingScreen.txtOW')}</Text>
            </TouchableOpacity>
          </View>
        </View>
        <ScrollView style={styles.scrollContainer}>
          <View>
            <View style={styles.formContainer}>
              <View style={styles.inputContainer}>
                <TextInput style={styles.input} placeholder="Enter the origin" />
              </View>
              <View style={styles.inputContainer}>
                <TextInput style={styles.input} placeholder="Enter destination" />
              </View>
              <View style={styles.inputContainer}>
                <TextInput style={styles.input} placeholder="Passengers" />
              </View>
              <View style={styles.inputContainer}>
                <TextInput style={styles.input} placeholder="Enter flight dates" />
              </View>
              <View style={styles.buttonChangeContainer}>
                <TouchableOpacity style={styles.buttonChange}>
                  <Image 
                  //source={Icon_arrows} 
                  style={styles.airplaneIcon} />
                </TouchableOpacity>
              </View>
            </View>
          </View>
          <View style={styles.currency}>
            <Image 
            //source={Icon_Currency} 
            style={styles.iconCurrency} />
            <Text style={styles.currencyText}>
              <Text style={styles.boldText}>Currency:</Text>  USD
            </Text>
          </View>

          <View style={styles.Pinmap}>
            <Image 
            //source={Icon_PinMap} 
            style={styles.iconCoupon} />
            <Text style={styles.codePromoText}>
              <Text style={styles.boldText}>Use your current location</Text>
            </Text>
          </View>

          <Boxs style={{ marginHorizontal: 20, marginVertical: 10 }}>
            <ScrollView showsVerticalScrollIndicator={true} style={{ height: 400, flex: 1 }}>
              <View style={styles.cardDestinations}>
                {destinations.map((destination, index) => (
                  <View style={styles.destination} key={index}>
                    <View>
                      <Image 
                      //source={Img_Example}
                      style={styles.destinationImage}  />
                    </View>
                    <View style={styles.destinationTitle}>
                      <Text style={styles.destinationCity}>{destination.city}</Text>
                      <Text style={styles.destinationCountry}>
                        <Text style={styles.smallText}>{destination.description}</Text>
                      </Text>
                    </View>
                    <View style={styles.destinationInitials}>
                      <Text style={styles.textEnd}>{destination.initials}</Text>
                    </View>
                  </View>
                ))}
              </View>
            </ScrollView>
          </Boxs>
        </ScrollView>
      </>
    )
  }

  const ReturnBookingIframe = () => {

    return (
      <>
        <HeaderGradient
          scrollY={scrollY}
          navigation={navigation}
          title={t('bookingScreen.txtTitle')}
          height={180}>
        </HeaderGradient>
        <View style={{ zIndex: 1 }}>
          <View style={styles.innerWrapper}>
            <Text style={styles.subTitle}>{t('bookingScreen.txtSubTitle')}</Text>
          </View>
        </View>
        <SafeAreaView style={{ flex: 1, top: 15 }}>
          <WebView
            source={{ uri: bookingUrl }}
          />
        </SafeAreaView>
      </>
    );
  }

  const styles = StyleSheet.create({
    innerWrapper: {
      marginTop: 60,
      paddingHorizontal: 16,
      zIndex: 1,
    },
    subTitle: {
      marginTop: 35,
      fontSize: 16,
      textAlign: 'center',
      color: fontColorPrimary,

    },
    tabMenu: {
      paddingTop: 10,
      flexDirection: 'row',
      justifyContent: 'space-around',
      marginTop: 5,
    },
    tabButton: {
      paddingHorizontal: 20,
      paddingVertical: 8,
      borderRadius: 5,
    },
    activeTab: {
      borderBottomColor: theme().root.bb_purple,
      borderBottomWidth: 4,
      fontSize: 10,
      color: theme().root.bb_white,
      fontFamily: theme().root.bb_font_bold,
    },
    tabButtonText: {
      fontSize: 16,
      color: theme().root.bb_white,
      fontFamily: theme().root.bb_font_bold,
    },
    buttonChangeContainer: {
      position: 'absolute',
      top: 50,
      right: 40,
      zIndex: 1,
    },
    buttonChange: {
      width: 55,
      borderRadius: 10,
      backgroundColor: theme().root.bb_purple,
      paddingVertical: 14,
      alignItems: 'center',
      justifyContent: 'center',
      marginBottom: 10,
      height: 45
    },
    button: {
      width: 340,
      borderRadius: 50,
      paddingVertical: 14,
      alignItems: 'center',
      justifyContent: 'center',
      marginBottom: 10
    },
    formContainer: {
      padding: 16,
    },
    inputContainer: {
      marginBottom: 10,
      zIndex: 1
    },
    input: {
      fontFamily: theme().root.bb_font_regular,
      backgroundColor: '#fff',
      borderRadius: 50,
      shadowColor: '#000',
      shadowOpacity: 0.2,
      shadowOffset: { width: 0, height: 2 },
      shadowRadius: 4,
      elevation: 2,
      fontSize: 16,
      color: '#444444',
      paddingLeft: 20,
      height: 50,
    },
    currency: {
      flexDirection: 'row',
      alignItems: 'center',
      paddingBottom: 10,
      paddingLeft: 20
    },
    iconCurrency: {
      height: 25,
      width: 25,
      tintColor: fontColorPrimary
    },
    currencyText: {
      marginLeft: 10,
      fontSize: 16,
      color: fontColorPrimary
    },
    boldText: {
      fontFamily: theme().root.bb_font_bold,
    },
    Pinmap: {
      flexDirection: 'row',
      alignItems: 'center',
      marginBottom: 10,
      paddingLeft: 20
    },
    iconCoupon: {
      height: 25,
      width: 25,
      tintColor: fontColorPrimary
    },
    codePromoText: {
      marginLeft: 10,
      fontSize: 16,
      color: fontColorPrimary,
    },
    cardDestinations: {
      marginBottom: 10,
      paddingLeft: 10,
      paddingVertical: 10
    },
    airplaneIcon: {
      width: 30,
      height: 30,
      tintColor: theme().root.bb_white,
    },
    destination: {
      flexDirection: 'row',
      alignItems: 'center',
      marginBottom: 10,
    },
    destinationImage: {
      height: 60,
      width: 92,
    },
    destinationTitle: {
      marginLeft: 10,
    },
    destinationCity: {
      fontFamily: theme().root.bb_font_bold,
      fontSize: 18,
    },
    destinationCountry: {
      paddingRight: 170
    },
    smallText: {
      fontFamily: theme().root.bb_font_regular,
      fontSize: 11,
    },
    destinationInitials: {
      marginLeft: 'auto',
      paddingRight: 10,
    },
    textEnd: {
      fontFamily: theme().root.bb_font_bold,
      fontSize: 15
    },
    scrollContainer: {
      flex: 1,
      marginTop: 15,
      marginBottom: 0,
    },
  });


  return (
    <Body>
      { booking ? ReturnBookingIframe() : ReturnBookingNative() }
    </Body>
  );
};


export default Booking;
