import { StackScreenProps } from '@react-navigation/stack';
import React from 'react';
import { View, Text, StyleSheet } from 'react-native';
import { theme } from '../../theme/theme';

interface Props extends StackScreenProps<any, any> {}

const FlightDetailScreen = () => {
  // Obtenemos los datos del vuelo desde las propiedades de navegación
  const flight = {
    origin: 'Bogotá',
    destination: 'Cartagena',
    date: '2023-07-15',
    status: 'En tiempo',
    airline: 'Aerolínea de Prueba',
    flightNumber: 'ABC123',
    gate: 'A1',
    terminal: 'T1',
    departureTime: '10:00 AM',
    arrivalTime: '12:00 PM',
    duration: '2h',
  };

  return (
    <View style={styles.container}>
      <Text style={styles.titleText}>{`${flight.origin} - ${flight.destination}`}</Text>
      <Text style={styles.subTitleText}>{`Fecha de vuelo: ${flight.date}`}</Text>
      <Text style={styles.subTitleText}>{`Estado de vuelo: ${flight.status}`}</Text>
      <Text style={styles.detailsText}>{`Aerolínea: ${flight.airline}`}</Text>
      <Text style={styles.detailsText}>{`Número de vuelo: ${flight.flightNumber}`}</Text>
      <Text style={styles.detailsText}>{`Puerta de embarque: ${flight.gate}`}</Text>
      <Text style={styles.detailsText}>{`Terminal: ${flight.terminal}`}</Text>
      <Text style={styles.detailsText}>{`Hora de salida: ${flight.departureTime}`}</Text>
      <Text style={styles.detailsText}>{`Hora de llegada: ${flight.arrivalTime}`}</Text>
      <Text style={styles.detailsText}>{`Duración del vuelo: ${flight.duration}`}</Text>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 16,
    backgroundColor: '#fff',
  },
  titleText: {
    fontFamily: theme().root.bb_font_bold,
    fontSize: 24,
    marginBottom: 8,
  },
  subTitleText: {
    fontFamily: theme().root.bb_font_regular,
    fontSize: 18,
    marginBottom: 4,
  },
  detailsText: {
    fontFamily: theme().root.bb_font_regular,
    fontSize: 16,
    color: '#444',
    marginBottom: 4,
  },
});

export default FlightDetailScreen;
