import React from 'react';
import { WebView } from 'react-native-webview'; 
import { SafeAreaView } from 'react-native-safe-area-context';
import Body from '../../components/General/Body';
import HeaderGradient from '../../components/General/HeaderGradient';
import { useSharedValue } from 'react-native-reanimated';

export function WebViewScreen({navigation}: any): JSX.Element {
  const scrollY = useSharedValue(0);
 
  return (
    <Body>
      <><HeaderGradient
            scrollY={scrollY}
            title="Web Check-In"
            height={0}
            navigation={navigation}></HeaderGradient><SafeAreaView style={{ flex: 1, top: 120 }}>
              <WebView
                source={{ uri: 'https://webcheckin.wingo.com/webcheckin/es/' }} />
            </SafeAreaView></>
    </Body>
   
  )
}


export default WebViewScreen;