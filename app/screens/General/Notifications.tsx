import {useState, useEffect} from 'react';
import {
  Text,
  View,
  StyleSheet,
  Image,
  TouchableOpacity,
} from 'react-native';

import {useSafeAreaInsets} from 'react-native-safe-area-context';
import {useSharedValue} from 'react-native-reanimated';

//import Icon_external_Link from '../../assets/icons/external-link.png';
//import Icon_Notifications from '../../assets/icons/icon-notification.png';

import {
  getListNotifications,
  marcarTododLeido,
  limpiarNotificacionesHistorial,
  aperturaNotificacion,
} from '../../helpers/notifications/notificationsHelper';

import {theme} from '../../theme/theme';
import Body from '../../components/General/Body';
import HeaderGradient from '../../components/General/HeaderGradient';
import ScrollView from '../../components/General/ScrollView';
import Boxs from '../../components/General/Boxs';
import Boton from '../../components/General/Boton';

//import Icon_Trips from '../../assets/icons/mytrips.png';
import {useTranslation} from 'react-i18next';
import { getRemoteConfig } from '../../helpers/firebase/firebase';

export function Notifications({navigation}: any): JSX.Element {
  const {t} = useTranslation();
  const scrollY = useSharedValue(0);
  const [loading, setLoading] = useState(true);
  const [dataNotificaciones, setDataNotificaciones] = useState<any>([]);

  useEffect(() => {
    obtenetNotificaciones();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const obtenetNotificaciones = async () => {
    const dataNotificacionesRecived = await getListNotifications();

    // Ordenar las noticicaciones recibidas en un objeto
    let notificationsShow = [];
    for (let i = dataNotificacionesRecived.orden.length - 1; i >= 0; i--) {
      const nuevaNotificationData = {
        titulo:
          dataNotificacionesRecived.notifications[
            dataNotificacionesRecived.orden[i]
          ].notification.notification.title,
        cuerpo:
          dataNotificacionesRecived.notifications[
            dataNotificacionesRecived.orden[i]
          ].notification.notification.body,
        id_notification: dataNotificacionesRecived.orden[i],
        url:
          dataNotificacionesRecived.notifications[
            dataNotificacionesRecived.orden[i]
          ].notification.data.url || '',
        imagen:
          dataNotificacionesRecived.notifications[
            dataNotificacionesRecived.orden[i]
          ].notification.data.image || '',
        leido:
          dataNotificacionesRecived.notifications[
            dataNotificacionesRecived.orden[i]
          ].leido,
        notification:
          dataNotificacionesRecived.notifications[
            dataNotificacionesRecived.orden[i]
          ].notification,
      };

      notificationsShow.push(nuevaNotificationData);
    }

    setDataNotificaciones(notificationsShow);
    setLoading(false);
    marcarTododLeido();
  };

  const limiparNotificaciones = () => {
    setDataNotificaciones([]);
    limpiarNotificacionesHistorial();
  };

  const views = dataNotificaciones.map((dato: any, index: any) => (
    <TouchableOpacity
      activeOpacity={0.5}
      style={[
        styles().contenedorNotification,
        index === dataNotificaciones.length - 1
          ? {
              borderBottomWidth: 0,
              borderBottomColor: '#ffffff',
              paddingBottom: 0,
            }
          : {},
      ]}
      key={`notification_${index}`}
      onPress={() => {
        aperturaNotificacion(dato.notification);
      }}>
      <View style={{flexDirection: 'column', flex: 1, position: 'absolute', left: 0, paddingLeft: 20, paddingTop: 30}}>
      <Image  resizeMode='cover' style={{width: 30, height: 30}}></Image>
      </View>
      <View style={{flexDirection: 'column', flex: 1, left: 80, marginRight: 80}}>
      <View style={styles().contenedorTitulo}>
        {!dato.leido && <View style={styles().indicadornoleido}></View>}
        <Text
          style={[
            styles().titulonotification,
            !dato.leido ? {marginLeft: 10} : {},
          ]}>
          {dato.titulo}
        </Text>
      </View>
      <Text
        style={[
          styles().bodynotification,
          !dato.leido ? {marginLeft: 20} : {},
        ]}>
        {dato.cuerpo}
      </Text>
      {dato.imagen !== '' && (
        <Image
          source={{
            uri: dato.imagen,
          }}
          resizeMode="cover"
          style={[styles().imgnotification]}></Image>
      )}
      {dato.url !== '' && (
        <View style={styles().contenedorDataURL}>
          <Image
            //source={Icon_external_Link}
            resizeMode="contain"
            style={{
              width: 18,
              height: 18,
              tintColor: theme().root.bb_orange,
            }}></Image>
          <Text style={styles().txtlinkUrl}>Go to Link</Text>
        </View>
      )}
      </View>
    </TouchableOpacity>
  ));

  return (
    <Body>
      <View style={styles().contenedorHeader}>
        <HeaderGradient
          scrollY={scrollY}
          title={t('notificationScreen.txtTitle')}
          height={0}
          navigation={navigation}></HeaderGradient>
      </View>
      <ScrollView scrollEnabled={true} style={styles().scrollContenedor}>
        <View style={styles().contenedorContenido}>
          <Boxs style={styles().boxiPrincipal}>
            {dataNotificaciones.length > 0 ? (
              views
            ) : (
              <View style={styles().contenedorSinNotificaciones}>
                <Image
                  //source={Icon_Notifications}
                  resizeMode="contain"
                  style={{
                    width: 65,
                    height: 65,
                    tintColor: theme().root.bb_gray,
                    marginBottom: 10,
                    marginTop: 10,
                  }}></Image>
                <Text style={styles().txtNoNotifications}>
                  {t('notificationScreen.txtNoPending')}
                </Text>
              </View>
            )}
          </Boxs>
          {dataNotificaciones.length > 0 && (
            <View style={{marginBottom: 16}}>
              <Boton
                nombreBtn={t('notificationScreen.btnClear')}
                disable={false}
                cargando={false}
                onPress={limiparNotificaciones}></Boton>
            </View>
          )}
        </View>
      </ScrollView>
    </Body>
  );
}

// Componente de estilos
const styles = () => {
  const {top} = useSafeAreaInsets();

  const [fontPrimary, setFontPrimary] = useState(theme().root.bb_font_regular);
  getRemoteConfig('fontPrimary')
      .then((value: any) => {
        setFontPrimary(value)
      })
      .catch((error) => {
        console.log(error);
      });

  return StyleSheet.create({
    contenedorHeader: {
      zIndex: 1,
    },
    scrollContenedor: {marginTop: top + 100, zIndex: 2,},
    contenedorContenido: {
      paddingHorizontal: 16,
      paddingTop: 7,
      alignItems: 'flex-start',
    },
    boxiPrincipal: {
      width: '100%',
      marginBottom: 16,
    },
    contenedorNotification: {
      width: '100%',
      marginBottom: 16,
      paddingBottom: 16,
      borderBottomWidth: 1,
      borderBottomColor: '#dddddd',
    },
    contenedorTitulo: {
      flexDirection: 'row',
      alignItems: 'flex-start',
      marginBottom: 5,
    },
    indicadornoleido: {
      width: 10,
      height: 10,
      backgroundColor: theme().root.bb_purple,
      borderRadius: 100,
      marginTop: 7,
    },
    titulonotification: {
      fontFamily: theme().root.bb_font_bold,
      color: theme().root.bb_dark,
      fontSize: 18,
    },
    bodynotification: {
      fontFamily: fontPrimary,
      color: theme().root.bb_dark,
      fontSize: 16,
    },
    imgnotification: {
      marginTop: 10,
      height: 160,
      width: '100%',
      borderRadius: 20,
    },
    contenedorDataURL: {
      flexDirection: 'row',
      alignItems: 'center',
      marginTop: 20,
    },
    txtlinkUrl: {
      fontFamily: fontPrimary,
      color: theme().root.bb_orange,
      fontSize: 18,
      marginLeft: 10,
    },

    contenedorSinNotificaciones: {
      width: '100%',
      alignItems: 'center',
      justifyContent: 'center',
    },
    txtNoNotifications: {
      textAlign: 'center',
      fontFamily: fontPrimary,
      color: theme().root.bb_gray,
      fontSize: 18,
      marginBottom: 10,
    },
  });
};

export default {Notifications};
