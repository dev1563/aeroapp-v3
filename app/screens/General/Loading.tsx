import React, { useState } from "react";
import { ActivityIndicator, StyleSheet, View } from "react-native";

const Loading = ({ isLoading }: any) => {
  const [loading, setLoading] = useState(isLoading);

  return (
    <View style={styles.container}>
      <ActivityIndicator
        size="large"
        color="blue"
        animating={loading}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
});

export default Loading;
