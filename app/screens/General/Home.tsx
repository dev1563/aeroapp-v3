
import { View, Text, Image, TextInput, StyleSheet, FlatList } from 'react-native';
import React, {   useEffect, useState } from 'react';
import { useSharedValue } from 'react-native-reanimated';

import { AuthContext } from '../../context/AuthContext/AuthContext';
//import ImageVideo from '../../assets/img/video.jpg';
//import Icon_Search from '../../assets/icons/icon-search.png';
//import ImagePerfil from '../../assets/img/profile.jpg';

import Body from '../../components/General/Body';
import HeaderGradient from '../../components/General/HeaderGradient';
import { Homestyles } from '../../theme/HomeTheme';
import ScrollView from '../../components/General/ScrollView';
import Boton from '../../components/General/Boton';
import { useForm } from '../../hooks/useForm';

import { useTranslation } from 'react-i18next';
import { SlidePoster } from '../../components/General/SlidePoster';
import { getRemoteConfig } from '../../helpers/firebase/firebase';
import { theme } from '../../theme/theme';
import Loading from '../General/Loading';
import DynamicCategory from '../../components/General/Categories';
import Offers from '../../components/General/Offers';
import Destinations from '../../components/General/Destinations';
import { PosterData, SectionVisible } from '../../interfaces/appInterfaces';




const Home = ({ navigation }: any) => {

  const [isFocus, setIsFocus] = useState(false);
  const [loading, setLoading] = useState(true);
  const [fontPrimary, setFontPrimary] = useState(theme().root.bb_font_regular);

  const { t } = useTranslation();
  const { search, onChange } = useForm({ search: '' });
  const scrollY = useSharedValue(0);
  //const { user, logOut } = useContext(AuthContext);


  const categoryData1 = {
    name: t('homeScreen.txtNameCat1'),
    items: [
      { icon: { uri: 'https://i.ibb.co/Rhg6625/mytrips.png' }, subtitleKey: 'homeScreen.txtSubCat1.1', route: 'SomeRoute1' },
      //{ icon: require('../../assets/icons/icon-check-in.png'), subtitleKey: 'homeScreen.txtSubCat1.2', route: 'SomeRoute2' },
      // Agrega más elementos según sea necesario
    ],
  };

  const offersData1 = {
    name: 'homeScreen.txtNameCat2',
    items: [
      //{ img: ImageVideo, titleKey: 'homeScreen.txtSubCat2.1title', paragraphKey: 'homeScreen.txtSubCat2.1Paragraph', route: 'https://www.bebolder.co/our-solutions/' },
      //{ img: ImageVideo, titleKey: 'homeScreen.txtSubCat2.1title', paragraphKey: 'homeScreen.txtSubCat2.1Paragraph', route: 'https://www.bebolder.co/our-solutions/' },
      // Agrega más elementos según sea necesario
    ],
  };

  const destinationsData1 = {
    name: 'homeScreen.txtNameCat3',
    items: [
      //{ img: ImageVideo, titleKey: 'homeScreen.txtSubCat3.1title', paragraphKey: 'homeScreen.txtSubCat3.1Paragraph', route: 'https://www.bebolder.co/our-solutions/' },
      //{ img: ImageVideo, titleKey: 'homeScreen.txtSubCat3.1title', paragraphKey: 'homeScreen.txtSubCat3.1Paragraph', route: 'https://www.bebolder.co/our-solutions/' },
      //{ img: ImageVideo, titleKey: 'homeScreen.txtSubCat3.1title', paragraphKey: 'homeScreen.txtSubCat3.1Paragraph', route: 'https://www.bebolder.co/our-solutions/' },
      // Agrega más elementos según sea necesario
    ],
  };

  const visible: SectionVisible = {
      Slide: true, 
      Categories: true, 
      Destinations: true, 
      Offers: true,
      // Agrega más elementos según sea necesario
  };

  const [categories, setCategories] = useState(categoryData1);
  const [offers, setOffers] = useState<PosterData>(offersData1);
  const [destinations, setDestinations] = useState<PosterData>(destinationsData1);
  const [Isvisible, setIsVisible] = useState<SectionVisible>(visible);
  const [fontColorPrimary, setfontColorPrimary] = useState(theme().root.bb_white);

 

  useEffect(() => {
    const fetchRemoteConfig = async () => {
      try {
        const font = await getRemoteConfig('fontPrimary');
        setFontPrimary(font);
        const cat: any = await getRemoteConfig('categories');
        setCategories(JSON.parse(cat));
        const off: any = await getRemoteConfig('offers');
        setOffers(JSON.parse(off));
        const dest: any = await getRemoteConfig('destinations');
        setDestinations(JSON.parse(dest));
        const vis: any = await getRemoteConfig('visible_section');
        setIsVisible(JSON.parse(vis));
        const fontColor: any = await getRemoteConfig('fontColorPrimary');
        setfontColorPrimary(fontColor);
        setLoading(false);
        //console.log(user)
      } catch (error) {
        console.error('Error fetching remote config:', error);
        setLoading(false);
      }
    };

    fetchRemoteConfig();
  }, []);

  const loadingPage = () => {
    return (
      <Loading isLoading={loading} />
    );
  };
  const styles = StyleSheet.create({
    roundImagen: {
      width: 42,
      height: 42,
      backgroundColor: fontColorPrimary,
      borderRadius: 50,
      position: 'absolute',
      marginLeft: 10,
      right: 10,
      justifyContent: 'center',
      alignItems: 'center',
  },
    imageRound: {
      width: 26, 
      height: 26,
      tintColor: theme().root.bb_light
  },
  })

  const homePage = () => {
    return (
      <Body>
        <View style={Homestyles.contenedorHeader}>
          <HeaderGradient
            scrollY={scrollY}
            height={180}
            title={t('homeScreen.txtTitle')}
            navigation={navigation}
            toggle={true}
            notificationsData={true} />

          <View style={Homestyles.contenedorCampoHeader}>
            <View style={Homestyles.roundImagenHeader}>
              <Image 
              //source={ImagePerfil} 
              style={Homestyles.imageRoundHeader} 
              resizeMode="contain" />
            </View>
            <Text style={Homestyles.titulocampoHeader}>{t('homeScreen.txtGreeting')} carlos </Text>
            <Text style={[Homestyles.titulocampoFocusHeader, { fontFamily: fontPrimary }]}>{t('homeScreen.txtSubGreeting')}</Text>
          </View>

          <View style={[Homestyles.contenedorCampo, { marginTop: 130, marginRight: 16, position: 'absolute' }]}>

            <Text style={isFocus ? [Homestyles.titulocampoFocus, { fontFamily: fontPrimary }] : [Homestyles.titulocampo, { fontFamily: fontPrimary }]}>{t('homeScreen.inputSearch')}</Text>
            <TextInput
              style={[Homestyles.campoTexto, { fontFamily: fontPrimary }]}

              onChangeText={(value) => onChange(value, 'search')}
              value={search}

              onFocus={() => {
                setIsFocus(true);
              }}
              onBlur={() => {
                setIsFocus(false);
              }}

              autoCapitalize="none"
              autoCorrect={false}
              autoComplete="country"
              textContentType="countryName"
              returnKeyType="next"
            />
            <View style={styles.roundImagen}>
              <Image 
              //source={Icon_Search} 
              style={styles.imageRound} 
              resizeMode="contain" />
            </View>
          </View>
        </View>
        <ScrollView scrollEnabled={true} style={[Homestyles.scrollContenedor]}>
          <View style={Homestyles.contenedorCategories}>
            {Isvisible.Slide == true ? 
            (<View style={[{ top: 10, marginBottom: 40 }]}>
              <SlidePoster />
            </View>)
            :
            (<></>)}
            
            {Isvisible.Categories == true ? 
            ( <DynamicCategory
              categoryData={categories}
              fontPrimary={fontPrimary}
              t={t}
              navigation={navigation}
            />)
            :
            (<></>)}
           
          </View>

          <View style={{ marginVertical: 20 }} />
          
          {Isvisible.Destinations == true ? 
          (<View style={Homestyles.contenedorCategories}>
            <View style={Homestyles.titleContenedorCategories}>
                <Text style={Homestyles.titleCategories}>{t(destinations.name)}</Text>
            </View>
            <FlatList
                data={destinations.items}
                renderItem={ ({item}: any) => <Offers offers={item} />}
                keyExtractor={(index) => index.toString() }
                horizontal={true}
                showsHorizontalScrollIndicator={false}
            />
          </View>)
          :
          (<></>)}
          

          <View style={{ marginVertical: 5 }} />
          
          {Isvisible.Offers == true ?
          (<View style={Homestyles.contenedorCategories}>
            <View style={Homestyles.titleContenedorCategories}>
                <Text style={Homestyles.titleCategories}>{t(offers.name)}</Text>
            </View>
            <FlatList
                data={offers.items}
                renderItem={ ({item}: any) => <Destinations destinations={item} />}
                keyExtractor={(index) => index.toString() }
                horizontal={true}
                showsHorizontalScrollIndicator={false}
            />
          </View>)
          :
          (<></>)}
          

          <View style={{ marginVertical: 20 }} />

          <View style={Homestyles.contenedorCategories}>
            <View style={Homestyles.btnlogin}>
              <Boton
                nombreBtn={t('homeScreen.btnLogout')}
                cargando={loading}
                //onPress={logOut}
              />
            </View>
          </View>

          <View style={{ marginVertical: 20 }} />
        </ScrollView>

      </Body>
    );
  };

  return (
    !loading ? homePage() : loadingPage()
  );
};


export default Home;
