import React, {  useEffect, useState } from 'react';
import { Alert, Image, Keyboard, KeyboardAvoidingView, Platform, Pressable, StyleSheet, Text, TextInput, View } from 'react-native';
import { StackScreenProps } from '@react-navigation/stack';
import { useSharedValue } from 'react-native-reanimated';

import { useForm } from '../../hooks/useForm';
import { AuthContext } from '../../context/AuthContext/AuthContext';

//import Logo from '../../assets/img/bebolder.png';
//import Icon_Profile from '../../assets/icons/user.png';
//import Icon_Pass from '../../assets/icons/icon-pass.png';
//import Icon_Show from '../../assets/icons/icon-show.png';
//import Icon_Hide from '../../assets/icons/icon-hide.png';

import { Loginstyles } from '../../theme/LoginTheme';
import Body from '../../components/General/Body';
import HeaderGradient from '../../components/General/HeaderGradient';
import ScrollView from '../../components/General/ScrollView';
import Boton from '../../components/General/Boton';

import { useTranslation } from 'react-i18next';
import { getRemoteConfig } from '../../helpers/firebase/firebase';
import { theme } from '../../theme/theme';

interface Props extends StackScreenProps<any, any> { }

export const Login = ({ navigation }: Props) => {
  const scrollY = useSharedValue(0);
  const { t } = useTranslation();

  const handleScroll = (event: any) => {
    scrollY.value = event.nativeEvent.contentOffset.y;
  };

  //const { signIn, errorMessage, removeError, status } = useContext(AuthContext)
  const { email, password, onChange } = useForm({ email: '', password: '' });

  const [isFocus, setIsFocus] = useState(false);
  const [contrasenaVisible, setContrasenaVisible] = useState(false);
  const [loading, setLoading] = useState(false);

  const mostrarContrasena = () => {
    setContrasenaVisible(!contrasenaVisible);
  };

  const [PrincipalLogo, setPrincipalLogo] = useState();
  getRemoteConfig('PrincipalLogo')
    .then((value: any) => {
      setPrincipalLogo(value)
    })
    .catch((error) => {
      console.log(error);
    });

  const [fontColorPrimary, setfontColorPrimary] = useState(theme().root.bb_white);
  getRemoteConfig('fontColorPrimary')
    .then((value: any) => {
      setfontColorPrimary(value)
      setLoading(false);
    })
    .catch((error) => {
      setLoading(true)
      console.log(error);

    });

  useEffect(() => {
    //if (errorMessage.length === 0) return;
    setLoading(false);
    //Alert.alert('Login Incorrecto', errorMessage, [{ text: 'Ok', onPress: removeError }]);
  }, [])

  const onLogin = () => {
    Keyboard.dismiss();
    setLoading(true);
    //signIn({ correo: email, password });
  }
  const styles = StyleSheet.create({
    txtolvidocontrasena: {
      color: fontColorPrimary,
      fontSize: 16,
    },
    roundImagen: {
      width: 42,
      height: 42,
      backgroundColor: fontColorPrimary,
      borderRadius: 50,
      position: 'absolute',
      marginLeft: 10,
      justifyContent: 'center',
      alignItems: 'center',
    },
    imageRoundContrasena: {
      width: 24,
      height: 24,
      tintColor: fontColorPrimary,
    },
    btnlogin: {
      marginTop: 7,
      width: '100%',
    },
  })

  return (
    <>
      <Body>

        <HeaderGradient
          scrollY={scrollY}
          img={PrincipalLogo}
          navigation={navigation}
          styleIndex={16}></HeaderGradient>

        <ScrollView onScroll={handleScroll} scrollEnabled={true}>
          <KeyboardAvoidingView style={{ flex: 1 }} behavior={(Platform.OS === 'ios') ? 'padding' : 'height'}>
            <View style={Loginstyles.contenedorLogin} >

              <Text style={Loginstyles.tituloLogin}>{t('loginScreen.txtTitle')}</Text>

              <View style={Loginstyles.contenedorCampo}>
                <View style={styles.roundImagen}>
                  <Image 
                  //source={Icon_Profile} 
                  style={Loginstyles.imageRound} resizeMode="contain" />
                </View>
                <Text style={isFocus ? Loginstyles.titulocampoFocus : Loginstyles.titulocampo}>{t('loginScreen.inputUser')}</Text>
                <TextInput
                  style={[Loginstyles.campoTexto]}
                  keyboardType='email-address'

                  onChangeText={(value) => onChange(value, 'email')}
                  value={email}
                  onSubmitEditing={onLogin}

                  onFocus={() => {
                    setIsFocus(true);
                  }}
                  onBlur={() => {
                    setIsFocus(false);
                  }}

                  autoCapitalize='none'
                  autoCorrect={false}
                  autoComplete="email"
                  textContentType="emailAddress"
                  returnKeyType="next"
                />
              </View>

              <View style={Loginstyles.contenedorCampo}>
                <View style={styles.roundImagen}>
                  <Image 
                  //source={Icon_Pass} 
                  style={Loginstyles.imageRound} resizeMode="contain" />
                </View>
                <Text style={isFocus ? Loginstyles.titulocampoFocus : Loginstyles.titulocampo}>{t('loginScreen.inputPass')}</Text>
                <TextInput
                  style={[Loginstyles.campoTextoPassword]}
                  secureTextEntry={!contrasenaVisible ? true : false}

                  onChangeText={(value) => onChange(value, 'password')}
                  value={password}
                  onSubmitEditing={onLogin}

                  onFocus={() => {
                    setIsFocus(true);
                  }}
                  onBlur={() => {
                    setIsFocus(true);
                  }}

                  autoCapitalize='none'
                  autoCorrect={false}
                  autoComplete="current-password"
                  textContentType="password"
                  returnKeyType="go"

                />
                <View style={Loginstyles.contenedorMostrarContrasena}>
                  <Pressable
                    onPress={mostrarContrasena}
                    style={Loginstyles.btnMostrarContrasena}>
                    <Image
                      //source={contrasenaVisible ? Icon_Hide : Icon_Show}
                      style={styles.imageRoundContrasena}
                      resizeMode="contain"
                    />
                  </Pressable>
                </View>
              </View>

              <Pressable
                style={Loginstyles.btnOlvidoClave}
                onPress={() => {
                  //navigation.navigate('ForgotPassword');
                  console.log('Forgot Password');
                }}>
                <Text style={styles.txtolvidocontrasena}>{t('loginScreen.txtForgot')}</Text>
              </Pressable>

              <View style={styles.btnlogin}>
                <Boton
                  nombreBtn="Login"
                  cargando={loading}
                  onPress={onLogin}></Boton>
              </View>

            </View>
          
          <View style={Loginstyles.contenedorSignup}>
            <View style={Loginstyles.hrlinea}></View>
            <Text style={Loginstyles.txtnotaccount}>
              {t('loginScreen.txtSignup')}
            </Text>
            <View style={Loginstyles.btnRegistro}>
              <Boton
                nombreBtn={t('loginScreen.btnSignup')}
                height={45}
                cargando={false}
                onPress={() => {
                  navigation.replace('Register');
                }}></Boton>
            </View>
          </View>
          </KeyboardAvoidingView>
        </ScrollView>
      </Body>
    </>
  )
}

export default Login;