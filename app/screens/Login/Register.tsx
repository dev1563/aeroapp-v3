import React, {  useEffect, useState } from 'react';
import { Alert, Image, Keyboard, KeyboardAvoidingView, Platform, Pressable, Text, TextInput, View } from "react-native";
import { StackScreenProps } from '@react-navigation/stack';
import { useSharedValue } from 'react-native-reanimated';

import { AuthContext } from '../../context/AuthContext/AuthContext';
import { useForm } from '../../hooks/useForm';

//import Logo from '../../assets/img/bebolder.png';
//import Icon_Profile from '../../assets/icons/user.png';
//import Icon_Pass from '../../assets/icons/icon-pass.png';
//import Icon_Show from '../../assets/icons/icon-show.png';
//import Icon_Hide from '../../assets/icons/icon-hide.png';

import { Loginstyles } from '../../theme/LoginTheme';
import Body from '../../components/General/Body';
import HeaderGradient from '../../components/General/HeaderGradient';
import ScrollView from '../../components/General/ScrollView';
import Boton from '../../components/General/Boton';

import {useTranslation} from 'react-i18next';
import { getRemoteConfig } from '../../helpers/firebase/firebase';
import { theme } from '../../theme/theme';

interface Props extends StackScreenProps<any, any>{}

export const Register = ({ navigation }:Props ) => {

    const scrollY = useSharedValue(0);

    const {t} = useTranslation();

    const handleScroll = (event: any) => {
      scrollY.value = event.nativeEvent.contentOffset.y;
    };

    //const { signUp, errorMessage, removeError } = useContext( AuthContext );
  
    const { email, password, name, onChange } = useForm({ name: '', email: '', password: '' });

    const [isFocus, setIsFocus] = useState(false);
    const [contrasenaVisible, setContrasenaVisible] = useState(false);
    const [loading, setLoading] = useState(false);

    const [fontPrimary, setFontPrimary] = useState(theme().root.bb_font_regular);
    getRemoteConfig('fontPrimary')
      .then((value: any) => {
        setFontPrimary(value)
      })
      .catch((error) => {
        console.log(error);
      });

      const [fontColorPrimary, setfontColorPrimary] = useState(theme().root.bb_white);
      getRemoteConfig('fontColorPrimary')
        .then((value: any) => {
          setfontColorPrimary(value)
          setLoading(false);
        })
        .catch((error) => {
          setLoading(true)
          console.log(error);

        });

    const [PrincipalLogo, setPrincipalLogo] = useState();
    getRemoteConfig('PrincipalLogo')
      .then((value: any) => {
        setPrincipalLogo(value)
      })
      .catch((error) => {
        console.log(error);
      });

    const mostrarContrasena = () => {
      setContrasenaVisible(!contrasenaVisible);
    };
    
    useEffect(() => {
      //if(errorMessage.length === 0) return;
      setLoading(false);
      //Alert.alert( 'Registro Incorrecto', errorMessage, [{ text: 'Ok', onPress: removeError }] );
  
    }, [])

    const onRegister = () => {
        Keyboard.dismiss();
        setLoading(true);
        //signUp({
        //  nombre: name,
        //  correo: email,
       //   password
       // })
    }

    return (
        <>
        <Body>

        <HeaderGradient
          scrollY={scrollY}
          img={PrincipalLogo}
          navigation={navigation}
          styleIndex={16}></HeaderGradient>

          <ScrollView onScroll={handleScroll} scrollEnabled={true}>
          <KeyboardAvoidingView style={{ flex: 1 }} behavior={ (Platform.OS === 'ios') ? 'padding' : 'height' }>
          <View style={ Loginstyles.contenedorRegister }>
    
            <Text style={ [Loginstyles.tituloRegister, {fontFamily: fontPrimary}] }>{t('SignupScreen.txtTitle')}</Text>

            <View style={Loginstyles.contenedorCampo}>
            <View style={[Loginstyles.roundImagen, {backgroundColor: fontColorPrimary}]}>
                <Image 
                //source={Icon_Profile} 
                style={[Loginstyles.imageRound]} resizeMode="contain" />
            </View>
            <Text style={ isFocus ? [Loginstyles.titulocampoFocus, {fontFamily: fontPrimary}] : [Loginstyles.titulocampo, {fontFamily: fontPrimary}] }>{t('SignupScreen.inputUsername')}</Text>
            <TextInput
              style={[ Loginstyles.campoTexto, {fontFamily: fontPrimary} ]} 

              onChangeText={ ( value ) => onChange( value, 'name' ) }
              value={ name }
              onSubmitEditing={ onRegister }

              onFocus={() => {
                setIsFocus(true);
              }}
              onBlur={() => {
                setIsFocus(false);
              }}

              autoCapitalize='words'
              autoCorrect={ false }
              autoComplete="username"
              textContentType="nickname"
              returnKeyType="next"
            />
            </View>

            <View style={Loginstyles.contenedorCampo}>
            <View style={[Loginstyles.roundImagen, {backgroundColor: fontColorPrimary}]}>
                <Image 
                //source={Icon_Profile} 
                style={Loginstyles.imageRound} resizeMode="contain" />
            </View>
            <Text style={ isFocus ? Loginstyles.titulocampoFocus : Loginstyles.titulocampo }>{t('SignupScreen.inputUser')}</Text>
            <TextInput
              style={[ Loginstyles.campoTexto, {fontFamily: fontPrimary} ]} 
              keyboardType='email-address'

              onChangeText={ ( value ) => onChange( value, 'email' ) }
              value={ email }
              onSubmitEditing={ onRegister }

              onFocus={() => {
                setIsFocus(true);
              }}
              onBlur={() => {
                setIsFocus(false);
              }}

              autoCapitalize='none'
              autoCorrect={ false }
              autoComplete="email"
              textContentType="emailAddress"
              returnKeyType="next"
            />
            </View>

            <View style={Loginstyles.contenedorCampo}>
            <View style={[Loginstyles.roundImagen, {backgroundColor: fontColorPrimary}]}>
                <Image 
                //source={Icon_Pass} 
                style={Loginstyles.imageRound} resizeMode="contain" />
            </View>
            <Text style={ isFocus ? Loginstyles.titulocampoFocus : Loginstyles.titulocampo }>{t('SignupScreen.inputPass')}</Text>
            <TextInput 
              style={[ Loginstyles.campoTextoPassword, {fontFamily: fontPrimary} ]}
              secureTextEntry={!contrasenaVisible ? true : false}

              onChangeText={ ( value ) => onChange( value, 'password' )}
              value={ password }
              onSubmitEditing={ onRegister }

              onFocus={() => {
                setIsFocus(true);
              }}
              onBlur={() => {
                setIsFocus(true);
              }}

              autoCapitalize='none'
              autoCorrect={ false }
              autoComplete="current-password"
              textContentType="password"
              returnKeyType="go"
              
            />
            <View style={Loginstyles.contenedorMostrarContrasena}>
                <Pressable
                  onPress={mostrarContrasena}
                  style={Loginstyles.btnMostrarContrasena}>
                  <Image
                    //source={contrasenaVisible ? Icon_Hide : Icon_Show}
                    style={[Loginstyles.imageRoundContrasena, {tintColor: fontColorPrimary}]}
                    resizeMode="contain"
                  />
                </Pressable>
              </View>  
            </View>

            <View style={Loginstyles.btnRegister}>
              <Boton
                nombreBtn={t('SignupScreen.btnRegister')}
                cargando={ loading }
                onPress={ onRegister }></Boton>
            </View>
    
          </View>

          <View style={Loginstyles.contenedorReturnLogin}>
            <View style={Loginstyles.hrlinea}></View>
              <Text style={[Loginstyles.txtreadyaccount, {fontFamily: fontPrimary}]}>
                {t('SignupScreen.txtLogin')}
              </Text>
              <View style={Loginstyles.btnReturnLogin}>
                <Boton
                  nombreBtn={t('SignupScreen.btnLogin')}
                  height={45}
                  cargando={false}
                  onPress={() => {
                    navigation.replace('Login');
                  }}></Boton>
              </View>
          </View>
          </KeyboardAvoidingView>

          
          </ScrollView>
        </Body>
        </>
      )
}

export default Register;