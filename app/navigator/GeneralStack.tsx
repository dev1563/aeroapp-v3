import { createStackNavigator } from '@react-navigation/stack';
import FlightListScreen from '../screens/General/ListTrips';
import FlightDetailScreen from '../screens/General/DetailTrip';
import MyTrips from '../screens/General/MyTrips';

const Stack = createStackNavigator();

export function GeneralStack(): JSX.Element {
    // Manejo de vistas de cada Tab
    return (
        <Stack.Navigator>
            <Stack.Screen
                name="Trips"
                component={MyTrips}
                options={{ headerShown: false }}
            />
            <Stack.Screen
                name="ListTrips"
                component={FlightListScreen}
                options={{ headerShown: false }}
            />
            <Stack.Screen
                name="DetailTrip"
                component={FlightDetailScreen}
                options={{ headerShown: false }}
            />
        </Stack.Navigator>
    );
}

export default { GeneralStack };