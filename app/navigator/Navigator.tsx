import React, {  useLayoutEffect } from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import { AuthContext } from '../context/AuthContext/AuthContext';

import Login from '../screens/Login/Login';
import Register from '../screens/Login/Register';

import {
  inicioServicioNotificaciones,
} from '../helpers/notifications/notificationsHelper';
import { DrawerStack } from '../navigator/DrawerStack';



const Stack = createStackNavigator();

export const Navigator = () => {

  //const { status } = useContext(AuthContext);

  useLayoutEffect(() => {
    setTimeout(async() => {
      // Solicita los permisos de notificaciones
      await inicioServicioNotificaciones();
    }, 160);
  }, []);

  return (
    <Stack.Navigator
      screenOptions={{
        headerShown: false,
        cardStyle: {
          backgroundColor: 'white'
        }
      }}>
      {
        (status !== 'authenticated') 
          ? (
            <>
              <Stack.Screen name="Login" component={Login} />
              <Stack.Screen name="Register" component={Register} />
            </>
            ) 
          : (
             <Stack.Screen name="HomeScreen" component={DrawerStack} />
            )
        //<Stack.Screen name="HomeScreen" component={TabNavigation} />


      }
    </Stack.Navigator>

  );
}