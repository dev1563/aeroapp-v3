import { DrawerContentComponentProps, DrawerContentScrollView, createDrawerNavigator } from "@react-navigation/drawer";
import TabNavigation from "../components/TabNavigation";
import { Image, TouchableOpacity, View, Text, useWindowDimensions } from "react-native";
import SelectDropdown from 'react-native-select-dropdown';
import i18n from 'i18next';
import {initReactI18next} from 'react-i18next';
import en from '../translate/en.json';
import es from '../translate/es.json';
import { useEffect, useState } from "react";
import { getRemoteConfig } from '../helpers/firebase/firebase';
import { theme } from "../theme/theme";

const Drawer = createDrawerNavigator();
const resources = {
    En: {
      translation: en,
    },
    Es: {
      translation: es,
    },
  };
const countries = ["En", "Es"]

 export const DrawerStack = () => {
    
    return (
        <Drawer.Navigator
        screenOptions={{
            headerShown: false
        }}
        
        drawerContent={(props) => <MenuContent {...props} />}>
            <Drawer.Screen name="HomeTabScreen" options={{title: 'Home'}} component={TabNavigation} />
            <Drawer.Screen name="WebViewCI" options={{title: 'Settings'}} component={TabNavigation} />
        </Drawer.Navigator>
    );
}

const MenuContent = ( {navigation}:  DrawerContentComponentProps) => {
    const {width, height } = useWindowDimensions();
    const [menuDrawer, setMenuDrawer] = useState({ 
      titles: {
        opc1: 'Ejemplo',
        opc2: 'Ejemplo',
        opc3: 'Ejemplo',
        opc4: 'Ejemplo',
      },});
    const [fontColorPrimary, setfontColorPrimary] = useState(theme().root.bb_white);

    useEffect(() => {
        const fetchRemoteConfig = async () => {
        try {
            const menuRemote: any = await getRemoteConfig('menu_lateral');
            const menuConfig = JSON.parse(menuRemote);
            setMenuDrawer(menuConfig);
            const fontColor: any = await getRemoteConfig('fontColorSecundary');
            setfontColorPrimary(fontColor);
        }
        catch (error) {
            console.error('Error fetching remote config:', error);
        }
        };

        fetchRemoteConfig();
    }, []);
    return (
        <DrawerContentScrollView>
            <View style={{alignItems: "center", marginTop: 20}}>
                <Image 
                    //source={ImagePerfil}
                    style={{width: 150, height: 150, borderRadius: 100}}
                />
            </View>
            <View style={{marginVertical: 30, marginHorizontal: 50}}>
                <TouchableOpacity 
                style={{marginVertical: 10}}
                onPress={ () => navigation.navigate('HomeTabScreen')}>
                    <Text style={{fontSize: 20}}>{menuDrawer.titles.opc1}</Text>
                </TouchableOpacity>
                <TouchableOpacity 
                style={{marginVertical: 10}}
                onPress={ () => navigation.navigate('WebViewCI')}>
                    <Text style={{fontSize: 20}}>{menuDrawer.titles.opc2}</Text>
                </TouchableOpacity>
            </View>
            <View style={{ alignSelf:"center", marginTop:height - 400}}>
            <SelectDropdown
                data={countries}
                defaultButtonText={'Select language'}
                onSelect={(selectedItem, index) => {
                    i18n.use(initReactI18next).init({
                        resources,
                        lng: selectedItem,
                        fallbackLng: 'en',
                        compatibilityJSON: 'v3',
                        interpolation: {
                          escapeValue: false
                        }
                      });
                }}
                buttonTextAfterSelection={(selectedItem, index) => {
                    // text represented after item is selected
                    // if data array is an array of objects then return selectedItem.property to render after item is selected
                    return selectedItem
                }}
                rowTextForSelection={(item, index) => {
                    // text represented for each item in dropdown
                    // if data array is an array of objects then return item.property to represent item in dropdown
                    return item
                }}
            />
            </View>
            
        </DrawerContentScrollView>
    );
}