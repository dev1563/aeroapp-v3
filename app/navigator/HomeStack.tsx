import { createStackNavigator } from '@react-navigation/stack';
import Home from '../screens/General/Home';
import { Notifications } from '../screens/General/Notifications';
import WebViewScreen from '../screens/General/WebViewScreen';


const Stack = createStackNavigator();

export function HomeStack(): JSX.Element {
    // Manejo de vistas de cada Tab
    return (
        <Stack.Navigator>
            <Stack.Screen
                name="MyHome"
                component={Home}
                options={{ headerShown: false }}
            />
            <Stack.Screen
                name="Notifications"
                component={Notifications}
                options={{ headerShown: false }}
            />
            <Stack.Screen
                name="WebViewCI"
                component={WebViewScreen}
                options={{ headerShown: false }}
            />
        </Stack.Navigator>
    );
}

export default { HomeStack };