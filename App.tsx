import 'react-native-gesture-handler';
import React, { useEffect, useState } from 'react';
import { NavigationContainer } from "@react-navigation/native";
import { Navigator } from './app/navigator/Navigator';
import { AuthProvider } from './app/context/AuthContext/AuthContext';
import { foregroundNotifications, notificacionAbierta } from './app/helpers/notifications/notificationsHelper';
import Splash from './app/screens/General/Splash';
import './app/translate/index';
import { fetchRemoteConfig } from './app/helpers/firebase/firebase';

const AppState = ({ children }: { children: JSX.Element | JSX.Element[] }) => {
  return (
    <AuthProvider>
      {children}
    </AuthProvider>
  );
}

const App = () => {
  const [isLoading, setIsLoading] = useState(true);
 
  useEffect(() => {

    // Detecta aplicaciones recibidas en Foreground
    foregroundNotifications();
    // Detecta cuando una notificación ha sido abierta
    notificacionAbierta();
  }, []);

  useEffect(() => {
    const fetchData = async () => {
      try {
        
      } catch (error) {
        // Manejar errores, por ejemplo, puedes mostrar un mensaje de error
        console.error('Error fetching remote config:', error);
      } finally {
        // setIsLoading(false) se ejecutará inmediatamente, pero setTimeout establecerá el valor en false después de 3 segundos
        setTimeout(async () => {
          // Usar directamente fetchRemoteConfig
          const { isFetched } = await fetchRemoteConfig();
          setIsLoading(!isFetched);
        }, 3000);
      }
    };

    fetchData();
  }, []);

  const loadingPage = () => (
    <Splash setIsLoading={setIsLoading} />
  );

  return isLoading ? loadingPage() : (
    <NavigationContainer>
      <AppState>
        <Navigator />
      </AppState>
    </NavigationContainer>
  );
}

export default App;
